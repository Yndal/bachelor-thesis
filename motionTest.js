var bb = require('bonescript');
var stack = require('./bachelor/scripts/MyStack');

stack.push(100);
stack.push(200);
stack.push(300);
stack.push(400);
stack.push(500);
stack.push(600);
stack.push(700);
stack.push(800);
stack.push(900);
stack.push(1000);
/*console.log("pushed");

console.log("popped: " + stack.pop());

console.log("popped: " + stack.pop());
console.log("popped: " + stack.pop());
*/


function test(){
    var t= stack.pop();
    if(t == stack.EMPTY) return;
    setTimeout(function(){
        console.log("now: " + t);
        test(stack);
        },t);        
}


test(stack);