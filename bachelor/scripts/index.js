var SAMPLE_TABLE_NAME = "sampleTable";
var CONTENT_ID = 'content';
var MANUAL_CONTROL_ID = 'manualControlles';
var OVERVIEW_ID = 'sampleOverview';
var PHYS_OVERVIEW_ID = 'physOverview';
//    var TIMER_ID_NAME = 'timerId';
var XML_NS_SVG = "http://www.w3.org/2000/svg";

var CONTENT_SELECT = 'contentSelect';
var INSPECTIONS_SELECT = 'inspectionsSelect';
var FREQ_SELECT = 'freqSelect';

var PETRI_DISH_SIZE = 'petriDishSize';
var PETRI_SPACE = 'Petri dish space in between';

var _config;

//Table as a cache
var _tableCache;

var socket = io.connect();


//Update counters every half second
//setInterval(updateTable(), 500);

initialize();
function initialize(){
    startListeners();
    startupRrequests();   
}


function startListeners () {
socket.on('config',function(data){
   _config = data;
   console.log("Got config: " + _config);
});

socket.on('tableUpdated',function(data){
    _tableCache = data;
    console.log("Table updated: " + _tableCache);
    if(currentContentIs(OVERVIEW_ID))
        showOverview();
});

/*socket.once('tableCache', function(data){
    _tableCache = data;
});*/
}


function startupRrequests(){
    socket.emit('getConfig');
    socket.emit('getTableCache');
    console.log("Start up done");
}

function getConfig(s){
    switch(s){
        case PETRI_DISH_SIZE:
            return _config[0][1];
        case PETRI_SPACE:
            return _config[1][1];
        default:
            var arr = new Array();
            arr.push('Available values');
            for(var i = 0; i<_config.length; i++)
                arr.push(_config[i]);
            return undefined;
    }
}

/*


//Get current time as a string
function getCurrentTime(){
    var date = new Date();
    
    var sec = numToString(date.getSeconds());
    var min = numToString(date.getMinutes());
    var hour = numToString(date.getHours());
    
    var day = date.getDate();
    var month = date.getMonth()+1; //Month is zero-based
    var year = date.getFullYear();
    
    
    var s = "";
    s += day + "-" + month + "-" + year;
    s += " " + hour + ":" + min + ":" + sec;
    
    return s;
}

function stringToDate(s){
    //alert(new Date().toDateString());
 //   alert("main.js - stringToDate is not done!");
    var date = new Date();
    var m = date.setDate(12);
    
    return date;
}

function numToString(secs){
    return secs < 10 ? 0 + secs.toString() : secs;
}
*/

/*************************************************************************************
 * **************************************************************************************
 *************************************************************************************/
 
function startup(){
    showOverview();
    
}
 
 
    function moveX(dir) {
      if(dir == 'forward')
      socket.emit('moveX', 'forward');
      else if (dir == 'backward')
       socket.emit('moveX', 'backward');
      else
       console.log("moveX got unknown parameter: " + dir);
    }
    
    function moveY(dir){
     if(dir == 'forward')
      socket.emit('moveY', 'forward');
      else if (dir == 'backward')
       socket.emit('moveY', 'backward');
      else
       console.log("moveY got unknown parameter: " + dir);
    }
    
    function dispDish(){
        socket.emit('dispDish');
        
        
    }
    
    
    
    function addNewContent(content) {
        var contentDiv = document.getElementById(CONTENT_ID);
     
        while(contentDiv.hasChildNodes())
            contentDiv.removeChild(contentDiv.firstChild);
        contentDiv.appendChild(content);
    }
    
    function addSample(content, inspections, frequency){
        socket.emit('addSample',{content: content, inspections: inspections, frequency: frequency});
    }
    
    function checkElec(id){
        socket.emit('checkSample', id);
    }
    
    function discardSample(id){
        socket.emit('discardSample', id);
    }
    
    
    function showOverview(){
        var newContent = document.createElement('div');
        newContent.setAttribute('id',OVERVIEW_ID);
     
        addSampleRowTo(newContent);        
     
        //Create sample table
        var samplesHeader = document.createElement('h2');
        samplesHeader.innerHTML = "Samples";
        newContent.appendChild(samplesHeader);
         
        var table = document.createElement('table');
        table.setAttribute('id', 'sampleTable');
        table.setAttribute('cellspacing', 0);
        table.setAttribute('cellpadding', 0);
        newContent.appendChild(table);
     
        var tr = document.createElement('tr');
        table.appendChild(tr);
     
        var th1 = document.createElement('th');
        th1.innerHTML = "Sample ID";
        tr.appendChild(th1);
     
        var th2 = document.createElement('th');
        th2.innerHTML = "State";
        tr.appendChild(th2);
     
        var th3 = document.createElement('th');
        th3.innerHTML = "Time added";
        tr.appendChild(th3);
     
        var th4 = document.createElement('th');
        th4.innerHTML = "Next inspection";
        tr.appendChild(th4);
     
        var th5 = document.createElement('th');
        th5.innerHTML = "Inspections left";
        tr.appendChild(th5);
     
        var th6 = document.createElement('th');
        th6.innerHTML = "Time left";
        tr.appendChild(th6);
     
        var th7 = document.createElement('th');
        th7.innerHTML = "Actions";
        tr.appendChild(th7);
        
        if(typeof _tableCache == 'undefined'){
            addNewContent(newContent);
            return;
        }
        
        //Add table data
        var dishArray = new Array();
        console.log("TableCache: " + _tableCache);//.dishMatrix);
        for(var i=0; i<_tableCache.dishMatrix.length; i++)
            for(var ii=0; ii<_tableCache.dishMatrix[0].length; ii++)
                if(typeof (_tableCache.dishMatrix[i][ii].dish) != 'undefined'){
                    console.log("c:" + i + ",r:" + ii + " --- ");
                    console.log(_tableCache.dishMatrix[i][ii]);
                    dishArray.push(_tableCache.dishMatrix[i][ii].dish);
                }
        
        console.log("Dish array size: " + dishArray.length);
        for(var d=0; d<dishArray.length; d++){
            var dish = dishArray[d];
            
            // Insert a row into the table
            var newRow   = table.insertRow(table.rows.length);
            
            //Set row color
            newRow.className = (table.rows.length % 2 == 1) ? "odd" : "even";
            
            var newCells = new Array();
            var newTexts = new Array();
            //Create cells
            for(var i=0; i<table.rows[0].cells.length; i++)
            newCells[i]  = newRow.insertCell(i);
            
            
            newTexts[0] = document.createTextNode(dish.id);
            newTexts[1] = document.createTextNode(dish.content);
            newTexts[2] = document.createTextNode(dish.startTime);
            newTexts[3] = document.createTextNode(dish.inspectionsLeft);
            newTexts[4] = document.createTextNode(dish.insinspecTotal);
            newTexts[5] = document.createTextNode("time left??");
            
            //Append cells with the data
            for(var i=0; i<table.rows[0].cells.length-1; i++)
            newCells[i].appendChild(newTexts[i]);
            
            //Add action buttons as the last column
            var checkButton = document.createElement('button');
            checkButton.setAttribute('onclick', 'checkElec(' + dish.id + ')');
            checkButton.innerHTML = 'Check voltage';
            newCells[table.rows[0].cells.length-1].appendChild(checkButton);
            
            var discardButton = document.createElement('button');
            discardButton.setAttribute('onclick', 'discardSample(' + dish.id + ')');
            discardButton.innerHTML = 'Discard sample';
            newCells[table.rows[0].cells.length-1].appendChild(discardButton);
        }
    
        addNewContent(newContent);
    }
     
     
    function addSampleRowTo(content){
         //Create "add sample"
         var addHeader = document.createElement('h2');
         addHeader.innerHTML = 'Add sample';
         content.appendChild(addHeader);
         
         var contentLabel = document.createElement('label');
         contentLabel.innerHTML = 'Set content';
         content.appendChild(contentLabel);
         
         var contentSelect = document.createElement('select');
         contentSelect.setAttribute('id', CONTENT_SELECT);
         content.appendChild(contentSelect);
         
         var waterOption = document.createElement('option');
         waterOption.setAttribute('value','Water');
         waterOption.innerHTML = 'Water';
         contentSelect.appendChild(waterOption);
         
         var oilOption = document.createElement('option');
         oilOption.setAttribute('value','Oil');
         oilOption.innerHTML = 'Oil';
         contentSelect.appendChild(oilOption);
         
         var emptyOption = document.createElement('option');
         emptyOption.setAttribute('value','Empty');
         emptyOption.innerHTML = 'Empty';
         contentSelect.appendChild(emptyOption);
         
         var inspectionsLabel = document.createElement('label');
         inspectionsLabel.innerHTML = 'Inspections';
         content.appendChild(inspectionsLabel);
         
         var inspectionsSelect = document.createElement('select');
         inspectionsSelect.setAttribute('id',INSPECTIONS_SELECT);
         content.appendChild(inspectionsSelect);
         
         var fiveOption = document.createElement('option');
         fiveOption.setAttribute('value','5');
         fiveOption.innerHTML = '5';
         inspectionsSelect.appendChild(fiveOption);
         
         var tenOption = document.createElement('option');
         tenOption.setAttribute('value','10');
         tenOption.innerHTML = '10';
         inspectionsSelect.appendChild(tenOption);
         
         var twentyOption = document.createElement('option');
         twentyOption.setAttribute('value','20');
         twentyOption.innerHTML = '20';
         inspectionsSelect.appendChild(twentyOption);
         
         var fiftyOption = document.createElement('option');
         fiftyOption.setAttribute('value','50');
         fiftyOption.innerHTML = '50';
         inspectionsSelect.appendChild(fiftyOption);
         
         var seventyfiveOption = document.createElement('option');
         seventyfiveOption.setAttribute('value','75');
         seventyfiveOption.innerHTML = '75';
         inspectionsSelect.appendChild(seventyfiveOption);
         
         var hundredOption = document.createElement('option');
         hundredOption.setAttribute('value','100');
         hundredOption.innerHTML = '100';
         inspectionsSelect.appendChild(hundredOption);
         
         var insFreqLabel = document.createElement('label');
         insFreqLabel.innerHTML = 'Inspection frequency';
         content.appendChild(insFreqLabel);
        
        var inspFreqSelect = document.createElement('select');
        inspFreqSelect.setAttribute('id',FREQ_SELECT);
        content.appendChild(inspFreqSelect);
        
        var sec15Option = document.createElement('option');
        sec15Option.setAttribute('value', 15);
        sec15Option.innerHTML = '15 sec';
        inspFreqSelect.appendChild(sec15Option);
        
        var sec30Option = document.createElement('option');
        sec30Option.setAttribute('value', 30);
        sec30Option.innerHTML = '30 sec';
        inspFreqSelect.appendChild(sec30Option);
        
        var min1Option = document.createElement('option');
        min1Option.setAttribute('value', 60);
        min1Option.innerHTML = '1 min';
        inspFreqSelect.appendChild(min1Option);
        
        var min2Option = document.createElement('option');
        min2Option.setAttribute('value', 120);
        min2Option.innerHTML = '2 min';
        inspFreqSelect.appendChild(min2Option);
        
        var min5Option = document.createElement('option');
        min5Option.setAttribute('value', 300);
        min5Option.innerHTML = '5 min';
        inspFreqSelect.appendChild(min5Option);
        
        var addSampleButton = document.createElement('button');
        addSampleButton.setAttribute('onclick',"addSample(document.getElementById(CONTENT_SELECT).options[document.getElementById(CONTENT_SELECT).selectedIndex].value," +
                                                "document.getElementById(INSPECTIONS_SELECT).options[document.getElementById(INSPECTIONS_SELECT).selectedIndex].value," + 
                                                "document.getElementById(FREQ_SELECT).options[document.getElementById(FREQ_SELECT).selectedIndex].value)");
        addSampleButton.innerHTML = 'Add sample';
        content.appendChild(addSampleButton);
    }
 
 
    
    function showManualControl(){
     var newContent = document.createElement('div');
     newContent.setAttribute('id', MANUAL_CONTROL_ID);
     
     var xForButton = document.createElement('button');
     xForButton.setAttribute('onmousedown', "moveX('forward')");
     xForButton.innerHTML = 'Move x forward';
     newContent.appendChild(xForButton);
     
     var xBackButton = document.createElement('button');
     xBackButton.setAttribute('onmousedown', "moveX('backward')");
     xBackButton.innerHTML = 'Move x backward';
     newContent.appendChild(xBackButton);
     
     var yForButton = document.createElement('button');
     yForButton.setAttribute('onmousedown', "moveY('forward')");
     yForButton.innerHTML = 'Move y forward';
     newContent.appendChild(yForButton);
     
     var yBackButton = document.createElement('button');
     yBackButton.setAttribute('onmousedown', "moveY('backward')");
     yBackButton.innerHTML = 'Move y backward';
     newContent.appendChild(yBackButton);
     
     var dispButton = document.createElement('button');
     dispButton.setAttribute('onclick', "dispDish()");
     dispButton.innerHTML = 'Dispense dish';
     newContent.appendChild(dispButton);
     
     addNewContent(newContent);
    }
    var dishes;
    function showPhysOverview(){
        startupRrequests();
        
        socket.emit('getPhysOverview');//,'');//getTestDataForPhysOverview();
        socket.once('physOverview', function(dishTable){
            console.log(dishTable);
   dishes = dishTable;
        var newContent = document.createElementNS(XML_NS_SVG,'svg');
         newContent.setAttribute('id', PHYS_OVERVIEW_ID);
         newContent.setAttribute('width', dishTable.widthMm);
         newContent.setAttribute('height', dishTable.lengthMm);
         
         var svgTable = document.createElementNS(XML_NS_SVG,'rect');
         svgTable.setAttribute('width', (dishTable.widthMm));
         svgTable.setAttribute('height', (dishTable.lengthMm));
         svgTable.setAttribute('x', '0');
         svgTable.setAttribute('y', '0');
         svgTable.setAttribute('style','stroke:black;stroke-width:5;fill:blue');
         svgTable.setAttribute('stroke','black');
         svgTable.setAttribute('fill','blue');
         newContent.appendChild(svgTable);
         
         var fill = 'white';
         var stroke = 'green';
         var strokeWidth = '3';
         var r = parseInt(getConfig(PETRI_DISH_SIZE))/2;
         var dishes = dishTable.dishMatrix;
         console.log("dishes: " + dishes);
         for(var row=0; row<dishes.length; row++){
            for(var column=0; column<dishes[0].length; column++){
                var dishSpot = dishes[row][column];
          
              var dishSvg = document.createElementNS(XML_NS_SVG,'circle');
              dishSvg.setAttribute('cx', dishSpot.xMm);
              dishSvg.setAttribute('cy', dishSpot.yMm);
              dishSvg.setAttribute('r', r);
              dishSvg.setAttribute('fill', fill);
              dishSvg.setAttribute('stroke', stroke);
              dishSvg.setAttribute('stroke-width', strokeWidth);
              newContent.appendChild(dishSvg);
              console.log(dishSpot.dish);
              
              if (typeof (dishSpot.dish) == 'undefined') {
                  //Insert text with "undefined"
      /*            var offSetX = -14;
                  var offSetY = 2;
                  var textUndefined = document.createElementNS(XML_NS_SVG,'text');
                  textUndefined.setAttribute('x', dishSpot.xM+offSetX);
                  textUndefined.setAttribute('y', dishSpot.yMm+offSetY);
                  textUndefined.innerHTML = 'Empty';
                  newContent.appendChild(textUndefined);
        */          
              } else {
                  //Insert data
                  var offSetXId = -13;
                  var offSetYId = -10;
                  var textId = document.createElementNS(XML_NS_SVG,'text');
                  textId.setAttribute('x', dishSpot.xMm+offSetXId);
                  textId.setAttribute('y', dishSpot.yMm+offSetYId);
                  textId.innerHTML = 'Id: ' + dishSpot.dish.id;
                  newContent.appendChild(textId);
                  
                  //Content
                  var offSetXContent = -31;
                  var offSetYContent = 2;
                  var textContent = document.createElementNS(XML_NS_SVG,'text');
                  textContent.setAttribute('x', dishSpot.xMm+offSetXContent);
                  textContent.setAttribute('y', dishSpot.yMm+offSetYContent);
                  textContent.innerHTML = 'Cont.: ' + dishSpot.dish.content;
                  newContent.appendChild(textContent);
                  
                  //Inspections left
                  var offSetXMeas = -24;
                  var offSetYMeas = 14;
                  var textMeas = document.createElementNS(XML_NS_SVG,'text');
                  textMeas.setAttribute('x', dishSpot.xMm+offSetXMeas);
                  textMeas.setAttribute('y', dishSpot.yMm+offSetYMeas);
                  textMeas.innerHTML = 'Insp.: ' + dishSpot.dish.inspectionsLeft;// + '/' + dishSpot.dish.checkElAmountTotal;
                  newContent.appendChild(textMeas);
              }
            }
            
         }
        
         addNewContent(newContent);
        
        });
    
    }
    
function currentContentIs(id){
var element = document.getElementById(id);
if(typeof element != 'undefined')
    return true;
return false;
}
