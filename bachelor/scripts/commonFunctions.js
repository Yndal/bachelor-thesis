var _config = require('./config.js');

exports.mmToSteps = function (mm){
    return mm*6;//5.1513513513513513513513513513514;//(mm/_config.PITCH)*_config.STEPS_PER_ROTATION;
};

exports.stepsToMm = function (steps){
    return steps/6;//5.1513513513513513513513513513514;//(steps/_config.STEPS_PER_ROTATION)*_config.PITCH;
};


//Object declarings
exports.Pos = function (x,y){
    this.x = x;
    this.y = y;
};


var idCounter = 1; //ID for next sample
exports.Dish = function (content, inspections, frequenzy){
    this.id = idCounter++;
    this.content = content;
    this.startTime = new Date();//.today();
    this.inspectionsLeft = inspections;
    this.inspectionsTotal = inspections;
    this.measElArray = new Array();
    
    this.intervalId = "????����";
        /*setInterval(function (){
            if (this.nspectionsLeft <= 0) {
                clearInterval(this.intervalId);
            } else {
         //       this.measElArray[this.measElArray.length] = checkEl(this.id);
            }
        }, frequenzy);*/
};

//Represents a spot for a dish at the table
exports.DishTableSpot = function (x, y, dish) {
    this.x = x;
    this.y = y;
    this.pos = new exports.Pos(x,y);
    this.xMm = exports.stepsToMm(this.x);
    this.yMm = exports.stepsToMm(this.y);
    this.dish = typeof dish == 'undefined' ? undefined : dish;
};

//The overview of a table including dishes and size of table
exports.DishTable = function (width, length, dishMatrix){
    this.length = length; //y-axis at screen
    this.width = width; //x-axis at screen
    this.lengthMm = exports.stepsToMm(this.length);
    this.widthMm = exports.stepsToMm(this.width);
    this.dishMatrix = dishMatrix; //dishMatrix ==> DishTableSpot[r][c] ==> Dish
};


