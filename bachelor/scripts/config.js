//config


var _bone = require('bonescript');


/***************************
 * Configuration
 ***************************/
exports.MIN_STEP_DELAY = 0.003;

//Pins - Motor drivers
exports.ENA_X = "P8_16";
exports.DIR_X = "P8_17";
exports.STEP_X = "P8_18";

exports.ENA_Y = "P8_19";
exports.DIR_Y ="P8_11";
exports.STEP_Y = "P8_12";

exports.DRIVER_ENABLED_TIME = 5000;

exports.STEPS_PER_ROTATION = 200;//*16;
exports.PITCH = 30; //Mm pr rotation

//Tool pins
exports.PROBE_MEAS_PIN = "P8_10";


//End stops
exports.ENDSTOP_X_START = "P8_7"; //DONE
exports.ENDSTOP_X_STOP = "P8_8"; //DONE

exports.ENDSTOP_Y_START = "P8_9"; //DONE
exports.ENDSTOP_Y_STOP = "P8_10"; //DONE

exports.endstopXStopActivated = _bone.HIGH;
exports.endstopXStartActivated = _bone.HIGH;
exports.endstopYStopActivated = _bone.HIGH;
exports.endstopYStartActivated = _bone.HIGH;


//Dispenser related
exports.DISPENSER_ENDSTOP_FRONT = "P8_26";
exports.DISPENSER_ENDSTOP_BACK = "P8_14";

exports.DISPENSER_SERVO_CHANNEL = 0;
exports.SERVO_DISP_PIN = "P9_42";//"P8_13";
exports.DISP_SERVO_HZ = 50;

exports.DISPENSER_FRONT_ENDSTOP_PRESSED = _bone.HIGH;
exports.DISPENSER_BACK_ENDSTOP_PRESSED = _bone.HIGH;

var dispSpeedDiff = 0.0080; //Max is 0.0723
exports.SERVO_DISP_STOP_VALUE = 0.0723;
exports.SERVO_DISP_FORWARD_VALUE = this.SERVO_DISP_STOP_VALUE - dispSpeedDiff;
exports.SERVO_DISP_BACKWARD_VALUE = this.SERVO_DISP_STOP_VALUE + dispSpeedDiff;
exports.DISPENSER_SERVO_SPEED_BACKWARD = 2000;//1600;
exports.DISPENSER_SERVO_SPEED_STOP     = 1500;
exports.DISPENSER_SERVO_SPEED_FORWARD  = 1000;//1400;
exports.DISPENSER_SLEDGE_UPDATE_TIME = 100;


//DISPENSER - Positions at the platform
exports.DISPENSER_DISH_POS_X = 572;//1280;
exports.DISPENSER_DISH_POS_Y = 0;
exports.DISPENSER_TEMP_LID_POS_X = 0;//2050;
exports.DISPENSER_TEMP_LID_POS_Y = 0;

exports.DISCARD_POINT_X = 1000;
exports.DISCARD_POINT_Y = 1000;

//GRIPPER values
exports.GRIPPER_PIN = "P9_22";//"P9_16";
exports.GRIPPER_ARMS_PIN = 'P9_16';//"P9_22";
exports.GRIPPER_LIFTED = 0.015;
exports.GRIPPER_LOWERED = 0.122;//198; //2mm = 0.1229; 5mm=0.115; 10mm = 0.107; 15mm=0.099; 20mm=0.091; 25mm=0.083
exports.GRIPPER_LIFTING_SPEED = 20; //ms

exports.GRIPPER_ARMS_CLOSE = 0.08;
exports.GRIPPER_ARMS_OPEN = 0.03;
exports.GRIPPER_ARMS_SERVO_HZ = 50;

exports.GRIPPER_LOWERING_TIME = 1000; //ms
exports.GRIPPER_RAISING_TIME = 1000; //ms
exports.GRIPPER_SERVO_HZ = 50;
exports.ARMS_OPEN_TIME = 800; //ms
exports.ARMS_CLOSE_TIME = 800; //ms

exports.X_FORWARD = _bone.HIGH;
exports.X_BACKWARD = this.X_FORWARD == _bone.HIGH ? _bone.LOW : _bone.HIGH; //Make dependency to X_FORWARD
exports.Y_FORWARD = _bone.HIGH;
exports.Y_BACKWARD = this.Y_FORWARD == _bone.HIGH ? _bone.LOW : _bone.HIGH; //Make dependency to Y_FORWARD

//Speeds
exports.SPEED_EMPTY = 900; //mm/s
exports.SPEED_LIQUID = 100; //mm/s

exports.ACC_SPEED = 50; // mm/s^2
exports.DEC_SPEED = 50; // mm/s^2
exports.ACC_START_SPEED = 10; //mm/s
exports.DEC_START_SPEED = 10; //mm/s

exports.STEPPER_TIMING_DEFAULT = 10;

//Values
var hackSize = 1.2937062937062937062937062937063;
exports.PETRI_DISH_SIZE = 68/hackSize; //Diameter of a Petri dish in mm
exports.DISH_SPACE = 10/hackSize; //Space between dishes in mm


exports.extConfig = function(){
    var config = [
            ['Petri dish diameter',String(this.PETRI_DISH_SIZE)],
            ['Petri dish space in between',String(this.DISH_SPACE)]
            ];
    return config;
    
};


