var ROOT_PATH = "./bachelor/";//../";

var _app = require('http').createServer(handler);
var _io = require('socket.io').listen(_app);
var _fs = require('fs');
//var _bb = require('bonescript');
var _cl = require('./controlLayer.js');
var _path = require('path');
var _config = require('./config.js');
 
 
_app.listen(8090);

initialize();

//console.log("TEST1: " + typeof(function));
//console.log("TEST2: " + typeof(initialize));
function initialize(){
    //Nothing yet...
}

 
function handler (req, res) {
  var filePath = req.url == '/' ? _path.join(ROOT_PATH , 'index.html') : _path.join(ROOT_PATH , req.url);
  
 // _fs.writeFile("HERE I AM.TXT", "Yaaay");
  
  
  _fs.readFile(filePath, function (err, data) {
    if (err) {
      res.writeHead(500);
      console.log(err);
      res.end('Error loading ' + filePath);
      return;
    }
   
    res.writeHead(200);
    res.end(data);
  });
}
 


_io.sockets.on('connection', function (client) {
    
    client.on('getConfig', function(){
        var config = _config.extConfig();
        client.emit('config', config);
        
    });
    
  client.on('moveX', function(data){
    //console.log("MoveX recieved: " + data);
    if (data == 'forward') {
        _cl.moveOneStepX(_config.X_FORWARD);
        //code
    } else if (data == 'backward') {
        _cl.moveOneStepX(_config.X_BACKWARD);
        //code
    } else {
        //INVALID REQUEST!!
        console.log("MoveX invalid request recieved: " + data);
    }    
  });
    
  client.on('moveY', function(data){
    //console.log("MoveY received: " + data);
    if (data == 'forward') {
         _cl.moveOneStepY(_config.Y_FORWARD);
        //code
    } else if (data == 'backward') {
        _cl.moveOneStepY(_config.Y_BACKWARD);
        //code
    } else {
        //INVALID REQUEST!!
        console.log("MoveY invalid request recieved: " + data);
    }
  });
  
  client.on('dispDish', function(){
   // console.log("'dispDish' received");
    _cl.dispDish();
  });  
  
  client.on('addSample', function(data){
    if(_cl.addSample(data.content, data.inspections, data.frequency))
        updateTable();
  });
  
  client.on('getPhysOverview', function(){
        var table = _cl.getPhysOverview(); //getTestDataForPhysOverview();
      // console.log(table);
        client.emit('physOverview', table);
  });
  
    function updateTable(){
        var po = _cl.getPhysOverview();
        console.log("PO: " + po);
        _io.sockets.emit('tableUpdated', po);
        //client.broadcast.emit('tableUpdated', _cl.getPhysOverview());
    };
    
    client.on('getTableCache', function(){
        updateTable();        
    });
  
  client.on('discardSample', function(data){
      console.log("Got request to discard sample " + data);
      _cl.discardSample(data);
      
      updateTable();
       // console.log("discardSample() - id: " + data);
  });
  
  client.on('checkSample', function(data){
   //   console.log("Got request to measure sample " + data);
      _cl.checkEl(data);
  });
});
 
//_bb.run();
