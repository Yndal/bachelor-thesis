/****************************
 *
 * The BeagleBoard Layer
 * 
 ***************************/

var b = require('bonescript');
var async = require('async');
var config = require('./config.js');
var comFun = require('./commonFunctions.js');
var commands = require('./MyQueue.js');
//var dirQueue = require('./DirQueue.js');
var _initialized = false;
var _executingCommands = false;

var x=0;
var y=0;

/***************************
 * Initialization
 * *************************/
initialize();


/***************************
 * Tests
 * *************************/
/*function move5x5(){
    for(var a = 0; a<5; a++)
        this.moveX(config.X_FORWARD);
    
    for(var b = 0; b<5; b++)
        this.moveY(config.Y_FORWARD);
    
}
move5x5();*/



/*********************
 *Public functions
 *********************/


function initialize (){
    if(_initialized) return;
    //Set all pins to in/out
    b.pinMode(config.ENA_X, b.OUTPUT);
    b.pinMode(config.DIR_X, b.OUTPUT);
    b.pinMode(config.STEP_X, b.OUTPUT);
    //console.log("X-axis are done");
    
    b.pinMode(config.ENA_Y, b.OUTPUT);
    b.pinMode(config.DIR_Y, b.OUTPUT);
    b.pinMode(config.STEP_Y, b.OUTPUT);
    //console.log("Y-axis are done");
        
    b.pinMode(config.ENDSTOP_X_START, b.INPUT);
    b.pinMode(config.ENDSTOP_X_STOP, b.INPUT);
    b.pinMode(config.ENDSTOP_Y_START, b.INPUT);
    b.pinMode(config.ENDSTOP_Y_STOP, b.INPUT);
    //console.log("Endstops are done");
        
    b.pinMode(config.DISPENSER_ENDSTOP_FRONT, b.INPUT);
    b.pinMode(config.DISPENSER_ENDSTOP_BACK, b.INPUT);
    b.pinMode(config.SERVO_DISP_PIN, b.OUTPUT);
    //console.log("Dispenser is done");
    
    b.pinMode(config.GRIPPER_PIN, b.OUTPUT);
    b.pinMode(config.GRIPPER_ARMS_PIN, b.OUTPUT);
  //  console.log("Gripper is done");
    
    
    
    raiseProbe();
    
    openArms();
    raiseGripper();
    
    b.analogWrite(config.SERVO_DISP_PIN, config.SERVO_DISP_STOP_VALUE, config.DISP_SERVO_HZ);
               
    
    console.log("Initialization of BB Layer is done!");
    _initialized = true;
}

/*******************************************
 * Public functions
 * ****************************************/
 

//Move the x-axis ONE step in the given direction
//dir must be 0 or 1
exports.moveX = function (dir){
    commands.enqueue([function(){
        moveX(dir);}, config.STEPPER_TIMING_DEFAULT]);

    executeCommands();
};
//this.moveX(1);

//Move the y-axis ONE step in the given direction

exports.moveY = function (dir){
    commands.enqueue([function(){
        moveY(dir);}, config.STEPPER_TIMING_DEFAULT]);
        
    executeCommands();    
};


function calcSpeedProfile(steps){
    var arr = [];
    for(var i=0; i<steps; i++)
        arr[i] = config.STEPPER_TIMING_DEFAULT;
    
    return arr;    
}


exports.moveStepsX = function (steps){
    if(steps === 0)
        return;
    
    //TODO add speed profile calculations here
    console.log("Speed profile should be calculated here");
    console.log("x steps: " + steps);
    
  /*  var direction = -1;
    if(steps < 0){
        direction = config.X_BACKWARD;
        
    } else  {
        direction = config.X_FORWARD;
    }*/
    var dir = (0 < steps) ? config.X_FORWARD : config.X_BACKWARD;
   // console.log("XXXXXXXXXXXXXXXXXXXX: " + dir);
   
    var timings = calcSpeedProfile(steps);//config.STEPPER_TIMING_DEFAULT;
    
    var counter = 0;
    var helper = function(d){
        commands.enqueue([function(){
            moveX(d);},timings[counter]]);
        counter++;
        
        if(counter < Math.abs(steps)){
            helper(d);
         
        }
    };
    helper(dir);

/*    commands.enqueue([function(){
        moveX(dir);}, config.STEPPER_TIMING_DEFAULT]);
    if(steps===0 || Math.abs(steps)<1)
        executeCommands();
    else{
        if(steps < 0)
            this.moveStepsX(++steps);
        else
            this.moveStepsX(--steps);
    }
*/
  /* for(var i = 0; i<steps; i++){
        //dirQueue.enqueue(direction);
        commands.enqueue([(function (d){
            return function(){
                moveX(d);
            };
        }(dir)),timings[i]]);
    }
    executeCommands();
    */
};

exports.moveStepsY = function (steps){
    if(steps === 0) return;
    //TODO add speed profile calculations here
    console.log("Speed profile should be calculated here");
 
    console.log("y steps: " + steps);
    /*
    var dir = -1;
    if(steps < 0)
        dir = config.X_BACKWARD;
    else 
        dir = config.X_FORWARD;
    */
    var dir = (0 < steps) ? config.Y_FORWARD : config.Y_BACKWARD;
   // console.log("YYYYYYYYYYYYYYYYYYYY: " + dir);
    var timings = calcSpeedProfile(steps);//[config.STEPPER_TIMING_DEFAULT];
    
    var counter = 0;
    var helper = function(d){
        commands.enqueue([function(){
            moveY(d);},timings[counter]]);
        counter++;
        
        if(counter < Math.abs(steps)){
            helper(d);
         
        }
    };
    helper(dir);
    
    /*
    for(var i = 0; i<steps; i++){
    //    dirQueue.enqueue(dir);
        commands.enqueue([function(){
            moveY(dir);}, timings[i]]);
    }*/
    executeCommands();
};


exports.findDimensions = function(){
    //Stop everything that is going on!
    //_executingCommands = true;
    commands = [];
    commands = require('./MyQueue.js');
    
    //First let go of everything and lift the gripper
    openArms();
    raiseGripper();
    
    //Then move to origo
    
    var x = 0;
    var y = 0;
    
    async.series([
        function(){
            moveToOrigo();
        },
        function(){
            async.whilst(function(){
                return moveX(config.X_FORWARD);},
                function (callback){
                    x++;
                    setTimeout(callback, 1);},
                function(err){
                });
        }, function(){
            _executingCommands = false;
            commands = require('./MyQueue.js');
        }, function(){
            return new comFun.Pos(x,y);    
        }]);
        
                    
        /*x++;
  
    while(moveY(config.Y_FORWARD))
        y++;
    */
    
    
    
    
    
};

exports.moveToOrigo = function(callback){
    moveToOrigo(callback);
};




exports.dispDish = function(){
    commands.enqueue([function(){
        console.log("Dispensing dish");
        dispenseDish();}, 2000]);

    executeCommands();
};

exports.grabDish = function(){
    commands.enqueue([function(){
        console.log("Grabbing dish");}, 0]);
    commands.enqueue([function(){
        lowerGripper();
        }, config.GRIPPER_LOWERING_TIME]);
    commands.enqueue([function(){
        closeArms();
        }, config.ARMS_CLOSE_TIME]);
    commands.enqueue([function(){
        raiseGripper();
        }, config.GRIPPER_RAISING_TIME]);
    
    executeCommands();
};

exports.lowerGripper = function(){
    commands.enqueue([function(){
        console.log("SEFSDF");
        lowerGripper();
        }, config.GRIPPER_LOWERING_TIME]);
      
    executeCommands();
    
};

exports.raiseGripper = function(){
    commands.enqueue([function(){
        raiseGripper();
        }, config.GRIPPER_RAISING_TIME]);
      
    executeCommands();
    
};

exports.setDownDish = function(){
    commands.enqueue([function(){
        console.log("Sitting down dish");}, 0]);
    commands.enqueue([function(){
        lowerGripper();
        }, config.GRIPPER_LOWERING_TIME]);
    commands.enqueue([function(){
        openArms();
        }, config.ARMS_OPEN_TIME]);
    commands.enqueue([function(){
        raiseGripper();
        }, config.GRIPPER_RAISING_TIME]);
    
    executeCommands();
};

exports.lowerProbe = function(){
    commands.enqueue([function(){
        console.log("Lowering probe");},0]);
    commands.enqueue([function(){
        //Insert command here
        lowerProbe();
        }, 1000]);
    
    executeCommands();
};

exports.measureProbe = function(){
    commands.enqueue([function(){
        console.log("Measuring...");},0]);
    commands.enqueue([function(){
        //Insert command here
        measureProbe();
        }, 1000]);
        
     executeCommands();
};

exports.raiseProbe = function(){
    commands.enqueue([function(){
        console.log("Raising probe");},0]);
    commands.enqueue([function(){
        //Insert command here
        raiseProbe();
        }, 1000]);
    
    executeCommands();
};

exports.endstopXStopStatus = function(){
    var status = b.digitalRead(config.ENDSTOP_X_STOP) == b.HIGH;
    
    return status;
};
exports.endstopYStopStatus = function(){
    var status = b.digitalRead(config.ENDSTOP_Y_STOP) == b.HIGH;
    
    return status;
};
exports.endstopYStartStatus = function(){
    var status = b.digitalRead(config.ENDSTART_Y_STOP) == b.HIGH;
    
    return status;
};
exports.endstopXStartStatus = function(){
    var status = b.digitalRead(config.ENDSTART_X_STOP) == b.HIGH;
    
    return status;
};



/*function getEndStopsStatus(){
    var xstart = b.digitalRead(config.ENDSTOP_X_START);
    var xstop  = b.digitalRead(config.ENDSTOP_X_STOP);
    var ystart = b.digitalRead(config.ENDSTOP_Y_START);
    var ystop  = b.digitalRead(config.ENDSTOP_Y_STOP);
    
    alert("Is this working? getEndStopsStatus()");
    var endStopState = new Array(xstart, xstop, ystart, ystop);
        
    return endStopState;
}*/


/*********************
 * Private functions
 * ******************/

var enableTimerY;
function moveY(dir){
    if (dir == config.Y_FORWARD || dir == config.Y_BACKWARD){// return false;
        y+=dir;
        //console.log("X: " + x + " - Y: " + y);//dir: " + dir);
        
        //Check for status of endstops
        var endstop = dir == config.Y_FORWARD ? config.ENDSTOP_Y_STOP : config.ENDSTOP_Y_START;
        if (b.digitalRead(endstop) == b.HIGH){
     //       console.log("Endstop at Y is HIGH");
            return false;
        }
    //    console.log("Perfoming move YYY");
        
        b.digitalWrite(config.ENA_Y, b.LOW);
        clearTimeout(enableTimerY);
        enableTimerY = setTimeout(function() {
            b.digitalWrite(config.ENA_Y, b.HIGH);   
            setTimeout(function(){
                clearTimeout(enableTimerY);}, config.DRIVER_ENABLED_TIME);
                console.log("Released motor Y");
        }, config.DRIVER_ENABLED_TIME);
        
        b.digitalWrite(config.DIR_Y, dir);
        b.digitalWrite(config.STEP_Y, b.HIGH);
        
       // console.log("MOVED");
        
        
        setTimeout(function (){
            b.digitalWrite(config.STEP_Y, b.LOW);
            }, config.MIN_STEP_DELAY);
        //Check for end stop values
    //    console.log("Moving y ONE step");
        
        
        //Check for any more commands - else move to origo    
        
        
        return true;
    }
    return false;
}

var enableTimerX;
var xxx = 0;
function moveX(dir, callback){
//    console.log("XXXXX");
    if (dir == config.X_FORWARD || dir == config.X_BACKWARD){// return false;
        x+=dir;
        xxx += !dir;
        //console.log("X: " + x + " - Y: " + y + " - xxx: " + xxx);// dir: " + dir);
    
        //Check for status of endstops
        var endstop = dir == config.X_FORWARD ? config.ENDSTOP_X_STOP : config.ENDSTOP_X_START;
        if (b.digitalRead(endstop) == b.HIGH){
 //           console.log("Endstop at X is HIGH");
            return false;
        }
        
    //    console.log("Perfoming move XXX");
        
        b.digitalWrite(config.ENA_X, b.LOW);
        clearTimeout(enableTimerX);
        enableTimerX = setTimeout(function() {
            b.digitalWrite(config.ENA_X, b.HIGH);  
            setTimeout(function(){
                console.log("Released motor X");
                clearTimeout(enableTimerX);}, config.DRIVER_ENABLED_TIME);
        }, config.DRIVER_ENABLED_TIME);
        
        b.digitalWrite(config.DIR_X, dir);
        b.digitalWrite(config.STEP_X, b.HIGH);
        
     //   console.log("MOVED");
        
        setTimeout(function (){
            b.digitalWrite(config.STEP_X, b.LOW);
            }, config.MIN_STEP_DELAY);
        
        
        //Check for end stop values
    //    console.log("Moving x ONE step");
        
        if(callback != undefined)
            callback();
        return true;
    }
    return false;
}

/*async.whilst(
    function () { return count < 5; },
    function (callback) {
        count++;
        setTimeout(callback, 1000);
    },
    function (err) {
        // 5 seconds have passed
    }
);*/

function moveToOrigo(callback1){
    async.whilst(
        function () { return moveX(config.X_BACKWARD);},
        function (callback) {
            setTimeout(callback, config.STEPPER_TIMING_DEFAULT);
        },
        function (err) {
            async.whilst(
                function () { return moveY(config.Y_BACKWARD);},
                function (callback) {
                    setTimeout(callback, config.STEPPER_TIMING_DEFAULT);
                },
                function (err) {
                    console.log("Carriage at origo");
                    callback1();
                }
            );
        }
    );
    
   // callback();
}

var _sledgeLock = false;
var dishesToDispense = 0;
function dispenseDish(){
    var SledgeDirection = {
        Forward:1,
        Stop:0,
        Backward:-1
    };

    dishesToDispense++;
    console.log("Dish requested - total: " + dishesToDispense);
    
    if (_sledgeLock)
    return;
    _sledgeLock = true;
    
 //   console.log("Dishes left: " + dishesToDispense);
    
    var sledgeMoveTimer;
    function moveSledgeHelper(dir){
        switch(dir){
            case -1:
                if(b.digitalRead(config.DISPENSER_ENDSTOP_BACK) == config.DISPENSER_BACK_ENDSTOP_PRESSED){
                    moveSledgeHelper(SledgeDirection.Stop);
                    return;
                } else {
                    b.analogWrite(config.SERVO_DISP_PIN, config.SERVO_DISP_BACKWARD_VALUE, config.DISP_SERVO_HZ);
                    //servos.setTarget(config.SERVO_DISP_CHANNEL, config.DISPENSER_SERVO_SPEED_BACKWARD);
//                    console.log("Sledge moving backward");
                }
                break;
            case 0:
               // clearTimeout(sledgeMoveTimer);
               b.analogWrite(config.SERVO_DISP_PIN, config.SERVO_DISP_STOP_VALUE, config.DISP_SERVO_HZ);
                //servos.setTarget(config.SERVO_DISP_CHANNEL, config.DISPENSER_SERVO_SPEED_STOP);
                dishesToDispense--;
//                        console.log("Sledge stoppped");
                if (0<dishesToDispense){
                    moveSledgeHelper(SledgeDirection.Forward);
                } else {
                    _sledgeLock = false;
                    return;
                }
                //break;
            case 1:
                if(b.digitalRead(config.DISPENSER_ENDSTOP_FRONT) == config.DISPENSER_FRONT_ENDSTOP_PRESSED){
                    moveSledgeHelper(SledgeDirection.Backward);
                    return;
                } else {
                    b.analogWrite(config.SERVO_DISP_PIN, config.SERVO_DISP_FORWARD_VALUE, config.DISP_SERVO_HZ);
                        //servos.setTarget(config.SERVO_DISP_CHANNEL, config.DISPENSER_SERVO_SPEED_FORWARD);
//                    console.log("Sledge moving forward");
                }
                break;
            default:
                console.log("ERROR: Unknown direction sent to sledge: " + dir);
                break;
        }
	
        sledgeMoveTimer = setTimeout(function(){
            moveSledgeHelper(dir);
            }, config.DISPENSER_SLEDGE_UPDATE_TIME);
        
    }
    moveSledgeHelper(SledgeDirection.Forward);    
}

function closeArms(){
    b.analogWrite(config.GRIPPER_ARMS_PIN, config.GRIPPER_ARMS_CLOSE, config.GRIPPER_ARMS_SERVO_HZ);
}

function openArms(){
    b.analogWrite(config.GRIPPER_ARMS_PIN, config.GRIPPER_ARMS_OPEN, config.GRIPPER_ARMS_SERVO_HZ);
}

function raiseGripper(){
    b.analogWrite(config.GRIPPER_PIN, config.GRIPPER_LIFTED, config.GRIPPER_SERVO_HZ);
}

function lowerGripper(){
    b.analogWrite(config.GRIPPER_PIN, config.GRIPPER_LOWERED, config.GRIPPER_SERVO_HZ);
}

function setDownDish(){
//    console.log("SetDownDish");
    
}

function lowerProbe(){
    console.log("Lowering probe");    
}

function measureProbe(){
    console.log("Probe is measuring...");
}

function raiseProbe(){
     console.log("Raising probe");   
}




function executeCommands(){
    commands.enqueue([function(){
        _executingCommands = false;}, 10]);
    
    if(_executingCommands) return;
    _executingCommands = true;
    function executeCommandsHelper(){
        
        var command = commands.dequeue();
        if(command == commands.EMPTY){
   //         _executingCommands = false;
            return;
        }
            
        setTimeout(function(){
   /*         console.log("comm: " + command);
            console.log("length: " + command.length);
            console.log("func: " + command[0]);
            console.log("time: " + command[1]);
     */       
            command[0]();
            
            executeCommandsHelper();
            //_executingCommands = false; 
        }, command[1]);
    }
    //_executingCommands = false;    
    executeCommandsHelper();
}
//this.moveX(1);
//this.dispDish();












 
