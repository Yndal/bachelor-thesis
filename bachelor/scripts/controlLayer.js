//Configuration
//Get scripts
var async = require('async');
var _bbl = require("./bbLayer.js");
var _config = require('./config');
var _comFuns = require('./commonFunctions.js');

var _initialized = false;

/*******************************************************
 * Constants
 * ****************************************************/
 var DISPENSER_SPOT_TEXT = "Dispenser spot";
 var TEMP_LID_PLACE_TEXT = "Temp. lid place";
 
//var TRASH_POS = new _comFuns.Pos(_config.DISCARD_POINT_X, _config.DISCARD_POINT_Y)



//Fields
var _curPos;
var _maxPos = new _comFuns.Pos(0,0); //Needed??

//The table containing the dishes
var _dishTable;
//List with all current samples (Add row and column to this!)
var _dishList;
/*
var _actions; //Actions to be executed 
var _prioActions; //Prioritiesed actions to be executed (will be executed before those in _actions)
*/



/******************************************************
 * TODOs
 * ***************************************************/
/*
    Use event to update stuff in webAPI! (Maybe socket.io?)




*/



/******************************************************
 * Objects
 * ***************************************************/
/*function Action(sampleId, task){
    this.DISCARD_SAMPLE = 'discardSample';
    this._task = task;
    this._sampleId = sampleId;
    this._moves;
    
    
    
    
    this.executes = function(){
        //Check equipment is raised and safe
        raiseAllEquipment();
        
        //Move to location
        var pos = getPos(sampleId);
        moveTo(pos);
        
        //Perform task
        switch (this._task){
            case this.DISCARD_SAMPLE:
                console.log("Discarding sample: " + sampleId);
                grabDish();
                moveTo(TRASH_POS, _config.SPEED_LIQUID);
                releaseDish();
                
                //Remove from tables and lists
                
                
                break;
                
                
            default:
                console.log("Unknown task requested: " + task);
                break;
        }
        
        //Raise used equipment
        raiseAllEquipment();
        
      
      
      //If no more actions => move home (??)
        //Not inhere
        //  _prioActions != undefined  ? _prioActions[0].execute() : (_actions != undefined) ? _actions[0].execute() : moveHome();
        };
    
    
    function raiseAllEquipment(){
        
    }
    
    function moveTo(pos, speed){
        var xDist = _curPos.x-pos.x;
        var yDist = _curPos.y-pos.y;
        
        var timingsX = calcSpeedTimings(xDist, speed);
        var timingsY = calcSpeedTimings(yDist, speed);
        
        
        
        
        
    }
}


function calcSpeedTimings(dist, wantedSpeed){
    //Used: http://www.dummies.com/how-to/content/how-to-calculate-time-and-distance-from-accelerati.html
    var stepsPerMm = _config.STEPS_PER_ROTATION / _config.PITCH;
    var startSpeed = _config.ACC_START_SPEED;
    var stopSpeed = _config.DEC_START_SPEED;
    
    var stepsTotal = dist*stepsPerMm;
    var accSeconds = (wantedSpeed - startSpeed)/_config.ACC_SPEED;
    var decSeconds = (wantedSpeed - stopSpeed)/_config.DEC_SPEED;
    var accSteps =  Math.round((startSpeed*accSeconds+0.5*_config.ACC_SPEED*(accSeconds*accSeconds))*stepsPerMm);
    var decSteps = accSteps;
    var middleSteps = stepsTotal - accSteps - decSteps;
    
    //If the distance is too short to get up to wanted speed
    if(middleSteps < 0){
        accSteps = stepsTotal/2;
        decSteps = stepsTotal/2;
    }
        
        
    var timingArr = new Array();
    
    
    
      
    function speedAcc(t){
        return startSpeed+_config.ACC_SPEED*t;
    }
    
    function speedDec(t){
        return wantedSpeed-_config.DEC_SPEED*t;
    }
     
    function accTiming(s){
        return 1/(speedAcc(s)*stepsPerMm);
    }
    
    function decTiming(s){
        return 1/(speedDec(s)*stepsPerMm);
    }
    
    //Add the accelerating steps  
    var tAcc = 0.0; //Seconds
    var timing = 0;
    var stepCounterAcc = 0;
    //accArr.push(startTiming);
    while(stepCounterAcc < accSteps){
        timing = accTiming(tAcc);
        timingArr.push(timing);
        
        tAcc += timing;
        
        stepCounterAcc++;
    }
    
    //Add the steps at the wanted speed
    var stepCounterMid = 0;
    var timingMid = 1/(wantedSpeed*stepsPerMm);
    while(stepCounterMid < middleSteps){
        timingArr.push(timingMid);
        middleSteps++;
    }
    
    
    //Add decelerating steps
    var tDec = decSeconds;
    var stepCountDec = 0;
    var timingDec = 0.0;
    var tempDecArr = new Array();
    while(stepCountDec < decSteps){
        timingDec = decTiming(tDec);
        tempDecArr.push(timingDec);
        
        tDec-=timingDec;
        
        stepCountDec++;
    }
    
    for(var i=0; i<tempDecArr.length; i++){
        timingArr.push(tempDecArr.pop());
    }

    return timingArr;
}

*/

/* Needed fields
 
 sampleArray (includes coordinates)
 idList => sampleArray

 
 //Config
 mm between petriDishes
  */
  
 


/**************************
 *Public functions
 *************************/
function initialize (){
    if(_initialized) return;
    async.series([
        function(callback){
            _dishList = [];
            console.log("Finding dimensions - please wait");
            findDimensions(callback);
        },
        function(callback){
            //setDimensions(_maxPos);   
            console.log("Dimensions found: " + _maxPos.x + " * " + _maxPos.y);
            calcDishTable(_config.PETRI_DISH_SIZE, _config.DISH_SPACE, _maxPos, callback);
        }]);
            
        
    /*var size =*/ /*new _comFuns.Pos(_comFuns.mmToSteps(500), _comFuns.mmToSteps(1000));//*/ //findDimensions(setDimensions); //Origo is set in here
    
    //Hack-ish
    // _curPos = new _comFuns.Pos(0,0);
     //   setDimensions(size);
      /*console.log("Dimensions found: " + _maxPos.x + " * " + _maxPos.y);
         
         
    _dishTable = calcDishTable(_config.PETRI_DISH_SIZE, _config.DISH_SPACE, _maxPos);
    console.log("Dish table calculated");
        */ 
    _initialized = true;
}
/**************************
 * Start-Up functions
 * ***********************/
initialize();






exports.moveOneStepX = function (dir){
    if (dir == _config.X_FORWARD)
        _curPos.x++;
    else if (dir == _config.X_BACKWARD)
        _curPos.x--;
    else
        //INVALID VALUE
        return;
        
    _bbl.moveX(dir);
    
 /*   console.log("cl: Moving x one step: " + dir);
    console.log("_cur.pos: (" + _curPos.x + "," + _curPos.y + ")");
    */
};

exports.moveOneStepY = function (dir){
    if (dir == _config.Y_FORWARD)
        _curPos.y++;
    else if (dir == _config.Y_BACKWARD)
        _curPos.y--;
    else
        //INVALID VALUE
        return;
        
    _bbl.moveY(dir);
    
/*    console.log("cl: Moving y one step: " + dir);
    console.log("_cur.pos: (" + _curPos.x + "," + _curPos.y + ")");
    */
};

exports.dispDish = function(){
    _bbl.dispDish();
};



exports.moveTo = function(x, y){
        var deltaX = x - _curPos.x;
        var deltaY = y - _curPos.y;
        
        console.log("Current X: " + _curPos.x);
        console.log("Current Y " + _curPos.y);
        console.log("DeltaX: " + deltaX);
        console.log("DeltaY: " + deltaY);
      _bbl.moveStepsX(deltaX);
        _bbl.moveStepsY(deltaY);
        
        _curPos.x = x;
        _curPos.y = y;
        
        

};


exports.addSample = function(content, inspections, frequency){
   // try{
        
        //Find available dish spot
        var dishSpot = findAvailableDishSpot();
        
        //If no available spot then return with with
        if(typeof(dishSpot) == 'undefined') 
            return false;
        
   //     console.log("Spot1: " + dishSpot.dish);
        var dish = new _comFuns.Dish(content, inspections, frequency);
        
     //   console.log("SECOND");
        dishSpot.dish = dish;
        _dishList.push(dish);
       // console.log("Spot2: " + dishSpot.dish);
        //move arm...
      
        _bbl.dispDish();
       
      
       //Move to new dish position at dispenser
       console.log("Move to dispenser");
       this.moveTo(_config.DISPENSER_DISH_POS_X, _config.DISPENSER_DISH_POS_Y);
       
       
       //Grab lid
       _bbl.grabDish();
       
       //Move to temp lid place
       console.log("Move to lid place");
       this.moveTo(_config.DISPENSER_TEMP_LID_POS_X, _config.DISPENSER_TEMP_LID_POS_Y);
       
       //Set down lid
       _bbl.setDownDish();
       
       //Move back to dispenser position
       console.log("Move back to dispenser");
       this.moveTo(_config.DISPENSER_DISH_POS_X, _config.DISPENSER_DISH_POS_Y);
       
       //Grab dish
       _bbl.grabDish();
       
       //Move to the position for the dish
       console.log("Move to dish position");
       this.moveTo(dishSpot.x, dishSpot.y);
       
       //Set down dish
       _bbl.setDownDish();
       
       //Move to temp lid place
       console.log("Move back to lid place");
       this.moveTo(_config.DISPENSER_TEMP_LID_POS_X, _config.DISPENSER_TEMP_LID_POS_Y);
       
       //Grab lid
       _bbl.grabDish();
       
       
       //Move to the position for the dish
       console.log("Move to dish position with lid");
       this.moveTo(dishSpot.x, dishSpot.y);
       
       //Set down lid
       _bbl.setDownDish();
       
        return true;   
};

exports.discardSample = function (id){
    var dishSpot = findDishTableSpot(id);
    /*console.log("discardSample: dishSpot.dish = " + dishSpot.dish);
    console.log("discardSample: dishSpot.x = " + dishSpot.x);
    console.log("discardSample: dishSpot.y = " + dishSpot.y);
*/    if((typeof(dishSpot.dish) == 'undefined') || dishSpot.dish.content == TEMP_LID_PLACE_TEXT || dishSpot.dish.content == DISPENSER_SPOT_TEXT) {
        console.log("Unable to discard sample " + id);
        return;
    }
    
    this.moveTo(dishSpot.x, dishSpot.y);
    
    //Grab lid
    _bbl.grabDish();
   
    //Move to discard point
    this.moveTo(_config.DISCARD_POINT_X, _config.DISCARD_POINT_Y);
   
    //Set down lid
    _bbl.setDownDish();
   
    //Move back to sample position
    this.moveTo(dishSpot.x, dishSpot.y);
    
    //Grab dish
    _bbl.grabDish();
    
    //Move to discard position
    this.moveTo(_config.DISCARD_POINT_X, _config.DISCARD_POINT_Y);
    
    //Set down dish
    _bbl.setDownDish();
   
    //Remove dish from table
    removeDishFromTable(id); //This could be optimised!
    _dishList[id] = undefined;
};

exports.checkEl = function (id){
    var dishSpot = findDishTableSpot(id);
    if(typeof(dishSpot) == 'undefined') {
        console.log("Unable to measure sample " + id);
        return;
    }
    
    this.moveTo(dishSpot.x, dishSpot.y);
    
    _bbl.lowerProbe();
    _bbl.measureProbe();
    _bbl.raiseProbe();
};


exports.getPhysOverview = function(){
    return _dishTable;
};




/***************************
 *Private functions below
 **************************/
function removeDishFromTable(id){
    for(var c=0; c<_dishTable.dishMatrix.length; c++)
        for(var r=0; r<_dishTable.dishMatrix[0].length; r++){
            if((typeof(_dishTable.dishMatrix[c][r].dish) != 'undefined') && (_dishTable.dishMatrix[c][r].dish.id == id)){
                _dishTable.dishMatrix[c][r].dish = undefined;
                return;   
            }
        }
}


function findDimensions(callback){
    //Move to origo
    //returnToHome();
    var xSteps = 0;
    var ySteps = 0;
    async.series([
        function(callback){
            _bbl.moveToOrigo(callback);},
        function(callback){
            _curPos = new _comFuns.Pos(0,0);//???
            var t = setInterval(function(){
                _bbl.moveX(_config.X_FORWARD);
                if(_bbl.endstopXStopStatus() == _config.endstopXStopActivated){
                    clearInterval(t);
                    callback();
                } else {
                    xSteps++;
                    }},_config.STEPPER_TIMING_DEFAULT);},
        function(callback){
            var t = setInterval(function(){
                _bbl.moveY(_config.Y_FORWARD);
                if(_bbl.endstopYStopStatus() == _config.endstopYStopActivated){
                    clearInterval(t);
                    callback();
                } else {
                    ySteps++;
                    }}, _config.STEPPER_TIMING_DEFAULT)},
/*function(callback){
            //Find steps at y-axis
            while(_bbl.moveY(_config.Y_FORWARD))
                ySteps++;
            console.log("YYYY: " + ySteps);    
            callback();},
  */      function(callback){
            var size = new _comFuns.Pos(xSteps, ySteps);
            console.log("Find dim in CL: x=" + xSteps + ", y=" + ySteps);
         //   _curPos = new _comFuns.Pos(0,0);
            _maxPos = size;
            console.log("Steps: x: " + xSteps + ", y: " + ySteps);
            var x = _comFuns.stepsToMm(_maxPos.x);
            var y = _comFuns.stepsToMm(_maxPos.y);
            console.log("Dimensions are: (" + x + "," + y + ") mm");
            callback();
        }, function(callback){
            _bbl.moveToOrigo(callback);},
        function(err){
            
            callback(); 
        }]);
    //Return to origo
    /*var dims = {x:5000, y:5000};
    async.series([
        function(callback){
            dims = _bbl.findDimensions();
            
            function helper(){
                console.log("DIMS: " + dims);
                if(dims == "s"){
                    setTimeout(function(){
                        console.log("Still waiting...");
                        helper();}, 100);
                } else {
                                console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

                    callback();
                }
            }
            
            helper();
        }, function(callback){
            _bbl.moveToOrigo();
            setTimeout(function(){
                callback();}, 3000);
        },
        function(callback){
            _curPos = new _comFuns.Pos(0,0);
            callback();
           // console.log(
            console.log("Dimensions: " + dims.x + " * " + dims.y);
           // returnToHome();
            
            return new _comFuns.Pos(dims.x, dims.y);
        }])*/
}

function findAvailableDishSpot(){
    //NB Weird order, but it is to keep the samples at the one end of the x-axis
    for(var r = 0; r<_dishTable.dishMatrix[0].length; r++){
        for( var c=0; c<_dishTable.dishMatrix.length; c++){
            if(typeof (_dishTable.dishMatrix[c][r].dish) == 'undefined'){
                console.log("SUCCESS: (" + c + "," + r + ")");
                return _dishTable.dishMatrix[c][r];
                
            }
        }
    }

    return undefined;
}

//Move header to origo (and set this, when we are here anyway)
function returnToHome(){
    //Move to origo
    _bbl.moveToOrigo();
    /*while(_bbl.moveX(_config.X_BACKWARD));
    while(_bbl.moveY(_config.Y_BACKWARD));
    */
    
    //Set origo, when we already know that it is where we are
    _curPos = new _comFuns.Pos(0,0);
}

function setDimensions(size){
    _maxPos = size;    
}

//Calculated as the bee cube pattern
function calcDishTable(dishSize, dishSpace, tableSize, callback){
    //Risk of overflow??
    var petriDia = _comFuns.mmToSteps(dishSize);///1,2937062937062937062937062937063;
    var space = _comFuns.mmToSteps(dishSpace);///1,2937062937062937062937062937063;
    
    console.log("p: " + petriDia + ", space: " + space);
    
    var x = petriDia/2 + space; //First spot and pos in steps
    var y = petriDia/2 + space; //First spot and pos in steps
    var delta = petriDia+space;
    var columns = Math.floor((tableSize.x-space)/delta); //Width
    var rows = Math.floor((tableSize.y-space)/delta); //Length
    
    console.log("ROWS: " + rows + ", COLUMNS: " + columns);
   
    var dishArray = [];// new Array(columns);//][columns]; //= new Array();
    
    for(var c=0; c<columns; c++){
        dishArray[c] = [];//new Array(rows);
        for(var r=0; r<rows; r++){
            dishArray[c][r] = new _comFuns.DishTableSpot(x + c*delta, y + r*delta);    
        }
    }
    
    //Add place for temp lid place and the dispenser position
    dishArray[0][0].dish = new _comFuns.Dish("Temp lid place", 0, 0);
    dishArray[1][0].dish = new _comFuns.Dish("Dispenser", 0, 0);
    dishArray[2][0].dish = new _comFuns.Dish("Dispenser", 0, 0);
    
    console.log("TESTTESTTEST: " + dishArray[1][0].dish);
    
    
    
 /*   dishArray[0][1].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[2][2].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[2][3].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[4][3].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[1][4].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[1][5].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[5][6].dish = new _comFuns.Dish("HACK", 42, 42);
    dishArray[3][7].dish = new _comFuns.Dish("HACK", 42, 42);
   */ 
    
    _dishTable = new _comFuns.DishTable(tableSize.x, tableSize.y, dishArray);
    callback();
}

//typeof (_dishTable.dishMatrix[c][r].dish) == 'undefined'

function findDishTableSpot(dishId){
    var dm = _dishTable.dishMatrix;//dishArray;
    for(var c=0; c<dm.length; c++){
        for(var r=0; r<dm[0].length; r++){
            if(typeof(dm[c][r].dish) != "undefined")
                if(dm[c][r].dish.id == dishId){
                    console.log("Found dish with id: " + dishId);
                    return dm[c][r];
                }
        }
    }
    return undefined;
}

//*******************************************
//* These functions are only for testing!!! *
//*******************************************
/*

exports.getTestDataForPhysOverview = function(){
    var usedSpot = new _comFuns.Dish('Water',60, 10); 
     var dishArray = [[new _comFuns.DishTableSpot(44, 44),
                    new _comFuns.DishTableSpot((122), (44)),
                    new _comFuns.DishTableSpot((200), (44)),
                    new _comFuns.DishTableSpot((278), (44)),
                    new _comFuns.DishTableSpot((356), (44)),
                    new _comFuns.DishTableSpot((434), (44)),
                    new _comFuns.DishTableSpot((512), (44)),
                    new _comFuns.DishTableSpot((590), (44)),
                    new _comFuns.DishTableSpot((668), (44)),
                    new _comFuns.DishTableSpot((746), (44)),
                    new _comFuns.DishTableSpot((824), (44)),
                    new _comFuns.DishTableSpot((902), (44))],
                    
                    [new _comFuns.DishTableSpot((44), (122), usedSpot),
                    new _comFuns.DishTableSpot((122), (122), usedSpot),
                    new _comFuns.DishTableSpot((200), (122)),
                    new _comFuns.DishTableSpot((278), (122)),
                    new _comFuns.DishTableSpot((356), (122)),
                    new _comFuns.DishTableSpot((434), (122)),
                    new _comFuns.DishTableSpot((512), (122)),
                    new _comFuns.DishTableSpot((590), (122)),
                    new _comFuns.DishTableSpot((668), (122)),
                    new _comFuns.DishTableSpot((746), (122)),
                    new _comFuns.DishTableSpot((824), (122)),
                    new _comFuns.DishTableSpot((902), (122))],
                    
                    [new _comFuns.DishTableSpot((44), (200)),
                    new _comFuns.DishTableSpot((122), (200)),
                    new _comFuns.DishTableSpot((200), (200)),
                    new _comFuns.DishTableSpot((278), (200)),
                    new _comFuns.DishTableSpot((356), (200)),
                    new _comFuns.DishTableSpot((434), (200)),
                    new _comFuns.DishTableSpot((512), (200)),
                    new _comFuns.DishTableSpot((590), (200)),
                    new _comFuns.DishTableSpot((668), (200)),
                    new _comFuns.DishTableSpot((746), (200)),
                    new _comFuns.DishTableSpot((824), (200)),
                    new _comFuns.DishTableSpot((902), (200))],
                    
                    [new _comFuns.DishTableSpot((44), (278)),
                    new _comFuns.DishTableSpot((122), (278)),
                    new _comFuns.DishTableSpot((200), (278)),
                    new _comFuns.DishTableSpot((278), (278)),
                    new _comFuns.DishTableSpot((356), (278)),
                    new _comFuns.DishTableSpot((434), (278)),
                    new _comFuns.DishTableSpot((512), (278)),
                    new _comFuns.DishTableSpot((590), (278)),
                    new _comFuns.DishTableSpot((668), (278)),
                    new _comFuns.DishTableSpot((746), (278)),
                    new _comFuns.DishTableSpot((824), (278)),
                    new _comFuns.DishTableSpot((902), (278))],
                    
                    [new _comFuns.DishTableSpot((44), (356)),
                    new _comFuns.DishTableSpot((122), (356)),
                    new _comFuns.DishTableSpot((200), (356)),
                    new _comFuns.DishTableSpot((278), (356)),
                    new _comFuns.DishTableSpot((356), (356)),
                    new _comFuns.DishTableSpot((434), (356)),
                    new _comFuns.DishTableSpot((512), (356)),
                    new _comFuns.DishTableSpot((590), (356)),
                    new _comFuns.DishTableSpot((668), (356)),
                    new _comFuns.DishTableSpot((746), (356)),
                    new _comFuns.DishTableSpot((824), (356)),
                    new _comFuns.DishTableSpot((902), (356))],
                    
                    [new _comFuns.DishTableSpot((44), (434)),
                    new _comFuns.DishTableSpot((122), (434)),
                    new _comFuns.DishTableSpot((200), (434)),
                    new _comFuns.DishTableSpot((278), (434)),
                    new _comFuns.DishTableSpot((356), (434)),
                    new _comFuns.DishTableSpot((434), (434)),
                    new _comFuns.DishTableSpot((512), (434)),
                    new _comFuns.DishTableSpot((590), (434)),
                    new _comFuns.DishTableSpot((668), (434)),
                    new _comFuns.DishTableSpot((746), (434)),
                    new _comFuns.DishTableSpot((824), (434)),
                    new _comFuns.DishTableSpot((902), (434))]];
     
    var table = new _comFuns.DishTable((1000), (500), dishArray);
 
    return table;
};*/

//*******************************************
//* END OF: These functions are only for testing!!! *
//*******************************************



