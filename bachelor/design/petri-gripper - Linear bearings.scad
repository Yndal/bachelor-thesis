space = 0.7;

petriDishSize = 58;
petriDishHeight = 15;

longCylDia = 20;
longCylHeight = 70; //From the bottom of the short cylinder
longInnerCylDia = 6;
longInnerCylHeight = longCylHeight;
holeDegree=60;


shortCylDia = petriDishSize*1.5;
shortCylHeight = 8;

arms = 4;
armWidth = 12;
armLength = 60;
armRaise = 21;
armScrewHole = 30;
armPullHoleDia = 2;
armPullHoleRaise = 5;

rubberTipHeight = 5;

armHolderThickness = 5;
armHolderScrewDia = 3;

teeths = 12;
teethSize = 5;

installProbes = true;
probeHoleDia = 6+0.5;
probeMountInDia = 9.5+0.7;
probeMountOutDia = 15;
probeMountHeight = 6; 
probeAndBbPlacement = 27;

res = 50;

//Ball bearing LME8UU
installBallBearings = true;

bbMountWidth = 24.5;
bbMountHeight = 31;
linearRailDia = 8;


//Servo details
servoPositionX = 1;
servoPositionY = 0;
servoPositionZ = 53;

topHoleDia = 2;

include <generics.scad>

	//mainUnit();


//translate([0,0,petriDishSize/2+rubberTipHeight+armWidth]) rotate([0,90,0])
//	arms();

//petri();


//LME8UU();

translate([0,0,0]) /*rotate([0,0,0]) */testMountForRails();
module testMountForRails(){
	difference(){
		union(){
			for(i=[0:arms-1]){
				if((i+1)%2 == 0){
					translate([0,0,bbMountHeight])
					rotate([180,0,i*(360/arms)+360/(arms*2)*2]){
						difference(){
							union(){
								translate([probeAndBbPlacement,0,0])
									cylinder(r=linearRailDia/2+2, h=bbMountHeight); 
								translate([probeAndBbPlacement,-10/2,0])
									cube([linearRailDia+4,10,bbMountHeight]);
								translate([-space,-linearRailDia/2,0])
									cube([probeAndBbPlacement+space, linearRailDia, bbMountHeight]);
							}
							//Rail
							translate([probeAndBbPlacement,0,-space])
								cylinder(r=linearRailDia/2, h=bbMountHeight+space*2); 
								
							//Slit
							translate([probeAndBbPlacement,-2/2,-space])
								cube([linearRailDia*2+space,2,bbMountHeight+space*2]);

							//Bottom slit
					/*		translate([probeAndBbPlacement+linearRailDia/2*0,-linearRailDia-space,-space])
						*/		cube([linearRailDia*2,linearRailDia*2,space*2]);

							//Space for strips
							translate([probeAndBbPlacement+linearRailDia/2+2,-linearRailDia/2-space-5,5])
								#cube([2,linearRailDia+10+space*2,5]);
							translate([probeAndBbPlacement+linearRailDia/2+2,-linearRailDia/2-space-5,bbMountHeight/2-5/2])
								#cube([2,linearRailDia+10+space*2,5]);
							translate([probeAndBbPlacement+linearRailDia/2+2,-linearRailDia/2-space-5,bbMountHeight-5*2])
								#cube([2,linearRailDia+10+space*2,5]);
							
							//Grip
							translate([-space*2,-linearRailDia/2-space,-space])
								cube([probeAndBbPlacement-linearRailDia/2-2+space, linearRailDia+space*2, bbMountHeight/2+space]);
						}
					}
				}
			}
		}
/*		for(i=[0:arms-1]){
			if((i+1)%2 == 0){
				rotate([0,0,i*(360/arms)+360/(arms*2)]){
					translate([probeAndBbPlacement,0,0]){
						hull(){
							translate([0,0,shortCylHeight])
								LME8UU();
							translate([probeAndBbPlacement+bbMountWidth,0,shortCylHeight])
								LME8UU();
						}
					
						translate([0,0,-space])
							cylinder(r=linearRailDia/2+space*1.5, h=shortCylHeight+bbMountHeight+space*2);
					}
				}
			}
		}
	*/}
}







//Test cube to get a feel of the size
//translate([50,0,-10]) cube(10);


/*local variables which can not be defined inside the methods*/
//teethDiagonal = sqrt(2*pow(teethSize,2));


/*Code for main piece starts here*/
module mainUnit(){
	
	union(){
		difference(){
			union(){
				//Teeth
/*				translate([-teethSize/2, -longCylDia/2, longCylHeight-20])
					for(i = [0:teeths])
						translate([0,0,-i*teethDiagonal])
							rotate([45,0,0]) 
								cube(teethSize, $fn=res);
	*/	
				cylinder(r=shortCylDia/2, h=shortCylHeight, $fn=res);
				cylinder(r=longCylDia/2, h=longCylHeight, $fn=res);
				
				//Servo mount
				translate([longInnerCylDia/2,-servoWidth/2-space/2,servoPositionZ-servoLength/2-servoMountLength-space*0])
					cube([(servoHeight+servoHeadHeight+2.5+2.4)+servoPositionX-servoHeight-1, servoWidth+space,servoLength+2*servoMountLength+space*2*0]);

				//Mount for probes
				if(installProbes){
					for(i=[0:arms-1]){
						if(i%2 == 0){
							rotate([0,0,i*(360/arms)+360/(arms*2)]){
								translate([probeAndBbPlacement,0,0])
									cylinder(r=probeMountOutDia/2, h=probeMountHeight);
							}
						}
					}
				}


				//Mounts for linear ball bearings
				if(installBallBearings){
					for(i=[0:arms-1]){
						if((i+1)%2 == 0){
							rotate([0,0,i*(360/arms)+360/(arms*2)]){
								translate([probeAndBbPlacement,0,0]) color("purple")
									cylinder(r=bbMountWidth/2, h=shortCylHeight+bbMountHeight); 
							}
						}
					}
				}


				//Arm holders
				for(i=[0:arms-1]){
					difference(){
						rotate([0,0,i*(360/arms)]){ 
							translate([shortCylDia/2-armWidth,-armWidth,0])
								color("green") cube([armWidth+rubberTipHeight,armWidth*2,armRaise]);
						}
						//Holes for screws
						rotate([90,0,i*(360/arms)]){
							translate([petriDishSize/2+armWidth/2+rubberTipHeight, armRaise-armWidth/2,-armWidth-1])
								cylinder(h=armWidth*2+2, r=armHolderScrewDia/2, $fn=res);
						}			
					}
				}
			}

			//Top hole
			translate([0,0,longCylHeight-20+space])
				cylinder(r=topHoleDia/2, h=20, $fn=12);

			//Space for the servo arm to move
			hull(){
				#translate([-longInnerCylDia/2-space,longInnerCylDia/2-18-2+2,servoPositionZ-servoLength/2+servoHeadDiameter/2+14.3+2])
					cube([longInnerCylDia+space*2,18+2,1]);
				#translate([-longInnerCylDia/2-space,longInnerCylDia/2-18-2+2,servoPositionZ-servoLength/2+servoHeadDiameter/2])
					cube([longInnerCylDia+space*2,18+2,1]);
				#translate([0,0,shortCylHeight])
					cylinder(r=longInnerCylDia/2, h=1);
			}

			//Hole for screw to the servo arm
			#translate([0,0,servoPositionZ-servoLength/2+servoHeadDiameter/2])
				rotate([0,270,0])
					cylinder(r=servoScrewHeadDiameter/2+space, h=longCylDia/2+space, $fn= 6);


			//Make space and wholes for servo at the mounting
			translate([servoHeight+servoHeadHeight+2.5+servoPositionX,servoPositionY,servoPositionZ])
				rotate([180,90,0])
					servo();
	
			//Make room for arms
			for(i=[0:arms-1]){
				rotate([0, 0, i*(360/arms)]){
					translate([longCylDia/2+((shortCylDia-longCylDia)/2)*0.2, -armWidth/2-space, -space])
						cube([shortCylDia/2, armWidth+space*2, 25]);
					translate([longCylDia/2, -armWidth/2-space, shortCylHeight])
						cube([shortCylDia/2, armWidth+space*2, 25]);	
				}
			}

			//Holes for the servo to interact with the arms
			translate([0,0,shortCylHeight]){
				//Hole in middle of tall cylinder	
			//	cylinder(r=longInnerCylDia/2, h=longInnerCylHeight+space, $fn=res);
				for(i=[0:arms-1]){
					difference(){
						rotate([0,holeDegree*0,i*(360/arms)])
							translate([-longInnerCylDia*tan(holeDegree),-longInnerCylDia/2,0])
								cube([longCylDia, longInnerCylDia, 25]);
/*						rotate([0,0,i*(360/arms)])
							translate([-100,-50,-50])
								cube(100);
				
	*/				}	
				}
			}

			//Space between arms
			for(i=[0:arms-1]){
				hull(){
					rotate([0, 0, i*(360/arms)]) translate([probeAndBbPlacement+bbMountWidth/2, -armWidth/2 -armWidth, -1])
						cube([shortCylDia,armHolderThickness, shortCylHeight+2]);
					rotate([0, 0, (i-1)*(360/arms) + 0]) translate([probeAndBbPlacement+bbMountWidth/2, -armWidth/2 + (armWidth*2-armHolderThickness), -1])
						cube([shortCylDia,armHolderThickness,shortCylHeight+2]);	
				}
			}
			
			//Add holes for probes
			if(installProbes){
				for(i=[0:arms-1]){
					if(i%2 == 0){
						rotate([0,0,i*(360/arms)+360/(arms*2)]){
							translate([probeAndBbPlacement,0,-space])
								cylinder(r=probeHoleDia/2, h=probeMountHeight/4+space*2, $fn=res);
							translate([probeAndBbPlacement,0,probeMountHeight/4])
							 	cylinder(r=probeMountInDia/2, h=probeMountHeight+2*space, $fn= 12);
	
							}
					}
				}
			}
			//Inside a difference()

			//Mounts for ball bearings and holes for linear rails
			if(installBallBearings){
				for(i=[0:arms-1]){
					if((i+1)%2 == 0){
						rotate([0,0,i*(360/arms)+360/(arms*2)]){
							translate([probeAndBbPlacement,0,0]){
								hull(){
									translate([0,0,shortCylHeight])
										LME8UU();
									translate([probeAndBbPlacement+bbMountWidth,0,shortCylHeight])
										LME8UU();
								}
							
								translate([0,0,-space])
									cylinder(r=linearRailDia/2+space*1.5, h=shortCylHeight+bbMountHeight+space*2);
							}
						}
					}
				}
			}
		}
	//Inside a union()
	}
}


/******Code for servo holder *****/




/******Code for the arms *****/
module arms(){
	for(i=[0:arms-1]){
		rotate([0,0,i*(360/arms)]){
			translate([petriDishSize/2, 0, -petriDishHeight]){
				//Round thing at the bottom
				rotate([0,90,0]){
					difference(){
						cylinder(h=5, r=petriDishHeight/2, $fn=res);
						translate([0,0,-0.01]) cylinder(h=0.5, r=petriDishHeight/2-1, $fn=res);
					}
				}
						
				//Might use to 2 "teeth" to keep the arm snapped on...
				difference(){
					//Arms
					translate([rubberTipHeight, -armWidth/2, -petriDishHeight/2]) 
						cube([armWidth, armWidth, armLength]);
	
					translate([rubberTipHeight+armWidth/2,0,-petriDishHeight/2+armLength-10])
						difference(){
							translate([-armWidth/2-1,-armWidth/2-1,0])
								cube([armWidth+2,armWidth+2,5]);
							cylinder(r=(petriDishSize/2+rubberTipHeight+armWidth)/2*0+armWidth/2-1, h=5, $fn=res);
						}
	
					//Cut corner of arms at bottom
					translate([rubberTipHeight,-armWidth/2-1,-petriDishHeight/2-armWidth*2]) rotate([0,-15,0])
						cube([armLength,armWidth+2,armWidth*2]);
	
					//Holes for screws
					translate([rubberTipHeight+armWidth/2,armWidth/2+20,armScrewHole])
						rotate([90,0,0])
							#cylinder(h=armWidth+2*20, r=armHolderScrewDia/2, $fn=res);


					//Holes for pulling
					translate([rubberTipHeight+armWidth+armPullHoleDia-1, 0, petriDishHeight/2+armPullHoleDia/2+armPullHoleRaise])
						rotate([0,-90+atan((petriDishHeight/2-armPullHoleRaise+shortCylHeight+15)/(petriDishSize/2+rubberTipHeight+armWidth+1)),0])
							#cylinder(h=armWidth+80, r=armPullHoleDia/2, $fn=res);
				}
			}
		}
	}
}



module petri(){
	translate([0,0,-petriDishHeight*1.5]) 
		%cylinder(r=petriDishSize/2, h=petriDishHeight);
}