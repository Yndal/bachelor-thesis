space = 1;
blob = 0.2;
res = 40;

//Please choose one or the other: For the cross or for linear bearings
forCross = true;
forLinearBearings = false;


//Cross related data
crossWidth = 5.5;
crossLength = 21;
crossSupportWidth = crossLength*2;
crossSupportLength = crossLength*2;
crossSupportHeight = 30;
crossSupportPosX = -40;
crossSupportPosY = 0;

//Linear bearigns design: Related data
railDia = 8;
railMountHeight = 30;
railMounThickness = 5;
railMountDist = 2;

//Probe data
probeDia = 4+2+space;
probeDist = 10;
probeDistFromCenter = 2;
//probe

//Belt data - for pulling carriage
beltMountWidth = 8;
beltMountLength = 30;
beltPositionZ = 25; //Measured from bottom of carriage
beltScrewDia = 4;

//Servo data
servoLength = 40;
servoWidth = 20.2+1.1;
servoHeight = 38;

servoMountLength = 7.4;
servoMountWidth = servoWidth;

servoHeadDiameter = 5.75;
servoHeadHeight = 3.3;

servoScrewDiameter = 1.8;
servoScrewHeadDiameter = 5.9;
servoScrewLength = 14;

servoPositionX = 20;
servoPositionY = 0;
servoPositionZ = -6;


//Stepper data
stepperLength = 36.2+space;
stepperWidth = 35+space*2;
stepperDDia = 5.00+space;
stepperDDiaShortest = 4.40+space;
stepperDLength = 19;

stepperScrewDist = 26;
stepperScrewDia = 3+space;
stepperCircleDia = 22+space*2;
stepperCircleHeight = 2;

stepperPosX = 6;
stepperPosY = 0;
stepperPosZ = 0;

stepperMountLength = 50;
stepperMountThickness = 3;

stepperBeltWidth = 8;			//Belt for lifting gripper
stepperBeltDepth = 3+5; 		//Belt for lifting gripper
stepperBeltDistFromCross = 4+3;	//Belt for lifting gripper
stepperBeltPullyDia = 23.87394556740411;		//Belt for lifting gripper - Ø=7.5cm
//31.83192742320548 - Ø=10.0cm
//Carriage data (The main unit)
carriageWidth = 180;
carriageHeight = 32;
carriageLength = 140;

carriageThickness = 6;
carriageSc12uuThickness = 4;

needles = 4; //Amount of needles at each side of the carriage
needlesWidthTop = 20;
needlesLengthTop = 25;
needlesWidthBottom = 15;
needlesLengthBottom = 20;
needlesPositionY = 70; //The center position of the needle from the side of the carriage

//SC12UU data
sc12uuLength = 35;
sc12uuWidth = 40+space*2;
sc12uuHeight = 26;
sc12uuScrewDistLength = 21;
sc12uuScrewDistWidth = 28;
sc12uuScrewDia = 5;


//Endstop data
endstopLength = 20+2;
endstopWidth = 6.5+1.5;
endstopHeight = 10.2;
endstopScrewDiameter = 3;
endstopScrewDistance = 9.5;

endstopMountWidth = 15;
endstopMountLength = endstopHeight;
endstopMountHeight = 30;

endstopPosX = 6.5;
endstopPosY = 0;
endstopPosZ = 0;

//translate([16.5,-5,36]) color("green")
//	cube([3,3,3]);




//translate([80,-55,5]) cube([10,34,10]);


//SC12UU();
module SC12UU(){
	#union(){
		color("Silver")
			translate([-sc12uuLength/2,-sc12uuWidth/2,0])
				cube([sc12uuLength,sc12uuWidth,sc12uuHeight]);
		color("black")
			translate([0,0,12])
				rotate([0,90,0])
					cylinder(r=10/2, h=sc12uuLength+space*2,center=true);
		for(a=[-1,1]){
			for(b=[-1,1]){
				translate([a*(sc12uuScrewDistLength/2), b*(sc12uuScrewDistWidth/2), -40])
					#cylinder(r=sc12uuScrewDia/2, h=(40+40)+sc12uuHeight, $fn=30);
			}
		}
	}		
}

//The gripper
/*include <petri-gripper - Cross.scad>
translate([carriageLength/2,0,-80]) mainUnit();
*/

carriage();
module carriage(){
	if(forCross && forLinearBearings){
		echo ("Please choose either for cross or linear bearigns - not both!");
	exit();
	}
//	translate([-carriageLength/2, -carriageWidth/2, 0])
{
		difference(){
			union(){
				difference(){
					translate([0, -carriageWidth/2, 0])
						cube([carriageLength, carriageWidth, carriageHeight]);
					//Main space in the middle
					translate([-blob, -(carriageWidth-sc12uuWidth*2-carriageThickness*2)/2,carriageThickness])
						cube([carriageLength+blob*2,carriageWidth-sc12uuWidth*2-carriageThickness*2, carriageHeight-carriageThickness+blob]);
				}
			
				//Add parts for the cross design
				if(forCross){
					translate([carriageLength/2-crossSupportLength/2 + crossSupportPosX,-crossSupportWidth/2+crossSupportPosY,carriageThickness])
						cube([crossSupportLength, crossSupportWidth, crossSupportHeight]);
					//Servo mount					
			/*		translate([carriageLength/2+crossSupportLength/2+crossSupportPosX,-crossSupportWidth/2+crossSupportPosY,carriageThickness])
						cube([20, crossSupportWidth, crossSupportHeight]);
			*/
					//Stepper mount
					translate([carriageLength/2+crossSupportPosX+crossLength/2,-crossSupportWidth/2+crossSupportPosY+stepperPosY-stepperBeltPullyDia/2,carriageThickness])
						cube([stepperMountLength+stepperPosX, crossSupportWidth, stepperWidth+crossSupportHeight+stepperPosZ]);

					//Endstop mount
					hull(){
						translate([endstopPosX+blob,-endstopMountWidth/2,carriageThickness+crossSupportHeight+endstopLength+endstopPosZ+3])
							#cube([endstopMountLength-blob*2, endstopMountWidth, 1]);

						translate([endstopPosX+blob, -endstopMountWidth/2,carriageThickness+crossSupportHeight])
							#cube([endstopMountLength-blob*2, endstopMountWidth, 1]);

						translate([carriageLength/2-crossSupportLength/2 + crossSupportPosX,-endstopMountWidth/2,carriageThickness+crossSupportHeight/2])
							#cube([0.1, endstopMountWidth, 0.1]);
					}

				}
			
				//Add parts for the linear bearing design
				if(forLinearBearings){
			
			
			
			
			
				}
			}

			//In "Difference"
			
			//Make carriage above the linear rails the right size (carriageSc12uuThickness)
			for(i=[-1,1])
				translate([-blob,i*(carriageWidth/2-sc12uuWidth-carriageThickness)-(sc12uuWidth+carriageThickness)/2+i*((sc12uuWidth+carriageThickness)/2)-blob,sc12uuHeight+carriageSc12uuThickness])
					cube([carriageLength+blob*2,sc12uuWidth+carriageThickness+blob*2,carriageThickness]);





			if(forCross){
				//Cross in cross support
				translate([carriageLength/2-crossLength/2+crossSupportPosX, -crossWidth/2+crossSupportPosY, -blob])
					cube([crossLength, crossWidth, carriageThickness+crossSupportHeight+blob*2]);
				translate([carriageLength/2-crossWidth/2+crossSupportPosX, -crossLength/2+crossSupportPosY, -blob])
					cube([crossWidth, crossLength, carriageThickness+crossSupportHeight+blob*2]);
	
/*				translate([carriageLength/2+servoHeight+servoPositionX, servoPositionY+5+2, carriageThickness+crossSupportHeight+servoLength/2+servoPositionZ+5+2])
					rotate([90,0,270])
						#servo();*/

				//Stepper mount
				translate([carriageLength/2+crossSupportPosX+crossLength/2-blob,stepperPosY-crossSupportWidth/2-stepperBeltPullyDia/2-blob,crossSupportHeight+carriageThickness])
					cube([(stepperLength+stepperCircleHeight+stepperDLength/2+crossLength+carriageLength/2+crossSupportPosX+stepperPosX)-stepperLength-stepperMountThickness-(carriageLength/2+crossSupportPosX+crossLength/2)-blob,crossSupportWidth+blob*2,stepperWidth+stepperPosZ+blob]);
				//Space for stepper
				translate([stepperLength+stepperCircleHeight+stepperDLength/2+crossLength+carriageLength/2+crossSupportPosX+stepperPosX,crossSupportPosY+stepperPosY-stepperBeltPullyDia/2,carriageThickness+crossSupportHeight+blob*2+stepperWidth/2+stepperPosZ])
					rotate([0,270,0]){
						#stepper();
						translate([0,0,45])
							#stepperWheel();
					}
				//Hole for stepper belt
				translate([carriageLength/2+crossLength/2+crossSupportPosX+stepperBeltDistFromCross-space,crossSupportPosY+stepperPosY-stepperBeltWidth/2-space,-blob])
					#cube([stepperBeltWidth+space*2,stepperBeltDepth+space*2, carriageThickness+crossSupportHeight+blob*2]);


			}

			if(forLinearBearings){
			
			
			}


			//Endstop for cross
			translate([endstopPosX,endstopPosY-endstopWidth/2,endstopPosZ+endstopLength+carriageThickness+crossSupportHeight+0*40])	
				rotate([0,90,0])
					#endstop();


			//Holes for probes
			for(a=[-1,1])
				for(b=[-1,1])
					translate([carriageLength/2+crossSupportPosX-crossSupportLength/2+10+a*probeDist, b*(crossSupportWidth/2 + crossSupportPosY + probeDia/2 + probeDistFromCenter),-space])
						#cylinder(r=probeDia/2, h=carriageThickness+space*2);


			//Space for SBRs
			translate([carriageLength-sc12uuLength/2+blob,-carriageWidth/2+sc12uuWidth/2-space,-blob])
				SC12UU();
			translate([sc12uuLength/2-blob,-carriageWidth/2+sc12uuWidth/2-space,-blob])
				SC12UU();
			translate([sc12uuLength/2-blob,carriageWidth/2-sc12uuWidth/2+space,-blob])
				SC12UU();
			translate([carriageLength-sc12uuLength/2+blob,(carriageWidth-sc12uuWidth)/2+space,-blob])
				SC12UU();
			
			//Space between SBRs at each side
			hull(){
				translate([-blob,-carriageWidth/2-space,-blob])
					cube([1,sc12uuWidth,sc12uuHeight]);
				translate([carriageLength+blob,-carriageWidth/2-space,-blob])
					cube([1,sc12uuWidth,sc12uuHeight]);
			}
			hull(){
				translate([sc12uuLength/2-blob,carriageWidth/2-sc12uuWidth+space,-blob])
					cube([1,sc12uuWidth,sc12uuHeight]);
				translate([carriageLength+blob,carriageWidth/2-sc12uuWidth+space,-blob])
					cube([1,sc12uuWidth,sc12uuHeight]);
			}



			//Space for needles
			translate([carriageLength-needlesLengthBottom-10, needlesWidthBottom/4, -blob])
				cube([needlesLengthBottom, needlesWidthBottom, carriageThickness+blob*2]);
			translate([carriageLength-needlesLengthBottom-10, needlesWidthBottom/4+needlesWidthBottom*1.5, -blob])
				cube([needlesLengthBottom, needlesWidthBottom, carriageThickness+blob*2]);

			translate([carriageLength-needlesLengthBottom-10, -needlesWidthBottom-needlesWidthBottom/4, -blob])
				cube([needlesLengthBottom, needlesWidthBottom, carriageThickness+blob*2]);
			translate([carriageLength-needlesLengthBottom-10, -needlesWidthBottom-needlesWidthBottom/4-needlesWidthBottom*1.5, -blob])
				cube([needlesLengthBottom, needlesWidthBottom, carriageThickness+blob*2]);

		/*	translate([carriageLength-needlesLengthBottom-10, -needlesWidthBottom/2, -blob])
				cube([needlesLengthBottom, needlesWidthBottom, carriageThickness+blob*2]);
			*/
			//Make space for needles
	/*		for(i=[0:needles-1]){
				for(s=[-1,1]){
					translate([i*(carriageLength/needles)+carriageLength/needles/2, s*(carriageWidth-needlesPositionY-sc12uuWidth-carriageThickness)/2, -blob])
{						
						hull(){
							translate([-needlesLengthTop/2,-needlesWidthTop/2,carriageHeight+blob])
								cube([needlesLengthTop, needlesWidthTop, blob]);
							
							if(s==-1)
								translate([-needlesLengthBottom/2, -needlesWidthTop*0+(needlesWidthTop-needlesWidthBottom) -needlesWidthTop/2,-blob])
									cube([needlesLengthBottom, needlesWidthBottom, blob]);					
							else
								translate([-needlesLengthBottom/2, -needlesWidthTop/2,-blob])
									cube([needlesLengthBottom, needlesWidthBottom, blob]);					
						}
						cylinder(r=1, h=100);
					
					}
				}
			}
*/			
	//	translate([0,0,0]) cube(50);
	
	
		}		
			
	
	}
}

//stepperWheel();
//Stepper wheel
module stepperWheel(){
	//The wheel
	translate([0,0,3])
		cylinder(r=stepperBeltPullyDia/2, h=stepperBeltWidth+space+3);
	translate([0,0,0])
		cylinder(r=stepperBeltPullyDia/2+(4+4)/2, h=3);
	translate([0,0,3+stepperBeltWidth+space+3])
		cylinder(r=stepperBeltPullyDia/2+(4+4)/2, h=3);
}
//!stepper();
module stepper(){
	difference(){
		//color("black")
			union(){
				translate([-stepperWidth/2,-stepperWidth/2,0])
					cube([stepperWidth,stepperWidth,stepperLength]);
				//Collar
				translate([0, 0, stepperLength-blob])
						cylinder(r=stepperCircleDia/2, h=stepperCircleHeight+blob, $fn=res);
				translate([0,0,stepperLength+stepperCircleHeight-blob])
						cylinder(r=stepperDDia/2, h=stepperDLength+blob, $fn=res);
	
			//Screw holes
			for(a=[-1,1])
				for(b=[-1,1])
					translate([a*stepperScrewDist/2,b*stepperScrewDist/2,stepperLength-blob])
						#cylinder(r=stepperScrewDia/2, h=15+blob, $fn=res);
	
		}
	}
}


//!servo();
module servo(){
	difference(){
		translate([-servoLength/2, -servoWidth/2,0]){
			union(){
				color("black"){
					translate([0,0,0])
						cube([servoLength, servoWidth, servoHeight]);
					
					//The mounting
					translate([-servoMountLength,(servoWidth-servoMountWidth)/2,27.5])
						cube([servoMountLength*2+servoLength,servoMountWidth,2.7]);
					//Arms
					translate([servoLength-10-servoHeadDiameter/2,servoWidth/2,servoHeight+servoHeadHeight])
						rotate([0,0,0])
							cylinder(r=38.4/2, h=2.4, $fn=30);
				}
				color("white"){
					translate([servoLength-10-servoHeadDiameter/2,servoWidth/2,servoHeight])
						cylinder(r=servoHeadDiameter/2, h=servoHeadHeight, $fn=30);
				}
				for(a=[-1,1]){
					for(b=[-1,1]){
						translate([servoLength/2+a*(servoLength/2+4.6), servoWidth/2+b*9.85/2,27.5-servoScrewLength]){
							cylinder(r=servoScrewDiameter/2, h=servoScrewLength*2+2.7, $fn=20);
						}
					}
				}
			}
		}
	}
}

module endstop(){
	difference(){
		union(){
			//body
			color("black")
				cube([endstopLength, endstopWidth, endstopHeight]);
		
			color("silver"){
				//switch
				translate([2.9,(endstopWidth-3.8)/2,10.2]) rotate([0,-10,0])
					cube([18.2, 3.8, 0.3]);
			
				//connectors
				translate([1.4,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([10,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([17.5,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);			
			}
		//Holes for screws
		for(i=[-1,1])
			translate([endstopLength/2+i*endstopScrewDistance/2,-50/2,3])
				rotate([-90,0,0])
					cylinder(r=endstopScrewDiameter/2, h=50, $fn=200);
		}

	}
}
