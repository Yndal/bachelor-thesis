outDia = 50;
wallThickness = 1.5;
bottomThickness = 2;
bottomHeight = 0.5;
height = 8; 

res = 300;

difference(){
	cylinder(h=height, r=outDia/2, $fn=res);
	translate([0,0,bottomThickness]) cylinder(h=height, r=(outDia/2)-wallThickness, $fn=res);
	
	translate([0,0,-1]) cylinder(h=bottomHeight+1, r=(outDia/2)-wallThickness, $fn=res);


}