space = 0.7;

///*** Endstop ***///
endstopLength = 20;
endstopWidth = 6.5;
endstopHeight = 10.2;
endstopScrewDiameter = 3;
endstopScrewDistance = 9.5;


module endstop(){
	difference(){
		union(){
			//body
			color("black")
				cube([endstopLength, endstopWidth, endstopHeight]);
		
			color("silver"){
				//switch
				translate([2.9,(endstopWidth-3.8)/2,10.2]) rotate([0,-10,0])
					cube([18.2, 3.8, 0.3]);
			
				//connectors
				translate([1.4,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([10,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([17.5,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);			
			}
		//Holes for screws
		for(i=[-1,1])
			translate([endstopLength/2+i*endstopScrewDistance/2,-50/2,3])
				rotate([-90,0,0])
					cylinder(r=endstopScrewDiameter/2, h=50, $fn=200);
		}

	}
}



///*** Servo ***///
/*servoLength = 23+1;
servoWidth = 12+2;
servoHeight = 16+1;
servoHeaderDiameter = 4.7+0.65; //0.65 becaue of mismatch between design and actual print
servoHeaderHeight = 2.6;
servoScrewsDist = 28; //Distance between wholes for the screws


servoScrewDiameter = 1.5;*/
servoLength = 23;
servoWidth = 12;
servoHeight = 21;

servoMountLength = 4.25;
servoMountWidth = 9;

servoHeadDiameter = 12;
servoHeadHeight = 3;

/*servoHeaderDiameter = 4.7+0.65; //0.65 becaue of mismatch between design and actual print
servoHeaderHeight = 2.6;
servoScrewsDist = 28; //Distance between wholes for the screws
*/

servoScrewDiameter = 1.5;
servoScrewHeadDiameter = 5.5;


module servo(){
	//This ommitted code is the actual size of the servo, while the next part (the code that is not ommitted is the servo with a buffer of <space> in size
	/*difference(){
		translate([-servoLength/2, -servoWidth/2,0]){
			union(){
				color("black"){
					translate([0,0,0])
						cube([servoLength, servoWidth, servoHeight]);
					translate([servoLength-servoHeadDiameter/2,servoWidth/2,servoHeight])
						cylinder(r=servoHeadDiameter/2, h=servoHeadHeight, $fn=30);
					//The mounting
					translate([-servoMountLength,(servoWidth-servoMountWidth)/2,16.5])
						cube([servoMountLength*2+servoLength,servoMountWidth,1]);
					//The other cylinder at the top
					translate([servoLength-(14.75-6/2),servoWidth/2,servoHeight])
						cylinder(r=6/2, h=servoHeadHeight+0.5, $fn=30);
				}
				color("white"){
					translate([servoLength-servoHeadDiameter/2,servoWidth/2,servoHeight+servoHeadHeight],$fn=20)
					cylinder(r=3.8/2, h=2.5);
				}
			}
		}
		for(i=[-1,1]){
			translate([i*(servoLength/2+2.4),0,-servoHeight/2]){
				cylinder(r=servoScrewDiameter/2, h=servoHeight*2.5, $fn=20);
			}
		}
	}*/
/*******************************************************************/
		translate([-servoLength/2, -servoWidth/2,0]){
			union(){
				color("black"){
					translate([-space,-space,0])
						cube([servoLength+space*2, servoWidth+space*2, servoHeight]);
					translate([servoLength-servoHeadDiameter/2,servoWidth/2,servoHeight-space])
						cylinder(r=servoHeadDiameter/2+space, h=servoHeadHeight+space, $fn=30);
					//The mounting
					translate([-servoMountLength-space,(servoWidth-servoMountWidth)/2-space,16.5])
						cube([servoMountLength*2+servoLength+space*2,servoMountWidth+space*2,1]);
					//The other cylinder at the top
					translate([servoLength-(14.75-6/2),servoWidth/2,servoHeight-space])
						cylinder(r=6/2+space, h=servoHeadHeight+0.5+space, $fn=30);
				}
				color("white"){
					translate([servoLength-servoHeadDiameter/2,servoWidth/2,servoHeight+servoHeadHeight],$fn=20)
					cylinder(r=(3.8+space)/2, h=2.5);
				}
			}
		}
		for(i=[-1,1]){
			translate([i*(servoLength/2+2.4),0,-servoHeight/2]){
				#cylinder(r=servoScrewDiameter/2, h=servoHeight*1.5+5, $fn=20);
			}
		}
		
	
}


//Ball bearing LME8UU

bbLength = 25;
bbInDia = 8;
bbOutDia = 16+0.5;

module LME8UU(){
	color("grey")
		cylinder(r=bbOutDia/2, h=bbLength);
}



