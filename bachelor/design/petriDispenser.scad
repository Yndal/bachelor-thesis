

include <extern/rack_herringbone.scad>
include <generics.scad>

/*** Globals ***/
size = 1;
res = 200; //Should be added to every figure drawn
space =1; //To avoid moving parts get stuck
pi = 3.1415;


/*** Main unit ***/
includeMounting = true;

mountingScrewDiameter = 8;
mountingScrewHeadDiameter = 21;
mountingThickness = 5;
mountingWidth = mountingScrewHeadDiameter*1.5;

platformSupportWidth = 10;
platformThickness = 3;

slitWidth = 20; //To ease adding extra dishes
slitEkstraSideLength = 20;

petriDishSize = 57.5; //Outter diameter
petriDishHeight = 15; //The heigth of the petri dish
//platformHeight = 1.5; //Where it will be attached
dispenserHeight = 150; //Amount of petri dishes in the dispenser
dispenserLength = 176;//200;//57.5*(3+1)+2*8;
dispenserThickness = 8; //Thickness of the dispenser's walls
dispenserSliderHeight = 30; //Height of the back part, where the slider is placed

sliderRailsRaise = petriDishHeight*0.2; //How much to raise the rails from the bottom
bottomRailsHeight = 2; //The rails at the bottom for the dishes

/*** Servo ***/
addServoHolder = true;
servoY = 95; //Placement on the y-axis - measured according to the servo's dimensions
servoX = 18.05; //Placement on the x-axis - measured according to the servo's dimensions
servoScrewsDist = 28; //Distance between wholes for the screws
servoWallThickness = 5;
servoBottomThickness = 0;
servoMountWidth = 12-0.5;


/*** Gear rack data ***/
innerRadius=3.1;//shaft radius, in mm
borders=2.5;//how thick should the borders around the central "shaft" be, in mm
diametralPitch=12;
numberOfTeeth=29;
pressureAngle=20*PI/180;
centerAngle=25;//angle at center of teeth


//gearDiameter = 25; //  
//servoGearWidth = 5;
//servoGearScrewDiameter = 1;

/*** Slider ***/
sliderWidth = petriDishSize+dispenserThickness;
sliderHeight = 3;
sliderLength = 172.5+15;

/*railWidth = 4;
railHeight = 3;*/
gearWidth = 4;
gearHeight = 3;
//gearAmount = 20;

//gearLessFront = 12; //millies in front to keep free of gears
//gearLessBack = 5; //millies at back to keep free of gears

switchBlockPlace = 57.5*3 - 10;
switchBlockWidth  = 10;
switchBlockHeight = 15;
switchBlockLength = 6;

rightRailLength = 137.5;//petriDishSize*3*0+35; //The right rail is used to activate endstop at the back - this defined when it should be activated

/*** Endstops ***/

endstop2x = -1;
endstop2y = 8;
endstop2z = -8;
endstop3x = 2;
endstop3y = 0;
endstop3z = 1;
/*************************************************************************************************/


//For servo mount calibration
//translate([-36.7,75,30]) color("blue")	cube(18);


//mainUnit();

//translate([50*0,0,0]) slider();
translate([150*0,0,0]) rotate([0,90,180]) servoGear();

/*translate([-13.3,70,35]) color("pink") cube(10);
translate([-13.3,36,35]) color("pink") cube(20);*/
//translate([0,0,0]) mainUnit();
//translate([-sliderWidth/2, 0.23*0-37, sliderRailsRaise+dispenserThickness]) slider();
//translate([2, servoY-servoWallThickness-5, dispenserSliderHeight+servoBottomThickness+servoWidth/2]) rotate([0,90,180]) servoGear();



/*  TODOs:
		- Holder til servo (præcis!!)
		Mulighed for at forlænge røret med skålene?
		
		
*/


/**************************************************************************************************/

//Main unit
module mainUnit()
	translate([-0.5*petriDishSize-dispenserThickness,-0.5*petriDishSize-dispenserThickness, 0]){
	difference(){
		union(){
			difference(){
				union(){
					cube([petriDishSize+2*dispenserThickness,petriDishSize+2*dispenserThickness,dispenserHeight+dispenserThickness], $fn=res);
					cube([petriDishSize+2*dispenserThickness, dispenserLength, dispenserSliderHeight], $fn=res);
					
					//Frame mounting
					translate([petriDishSize/2+dispenserThickness,0, dispenserThickness]) color("blue")
					for(i=[-1,1]){
						difference(){
							hull(){
								translate([i*(petriDishSize/2+dispenserThickness),0, 0])
									cube([1, mountingThickness, dispenserHeight - dispenserThickness]);
								translate([i*(petriDishSize/2+dispenserThickness)+i*mountingWidth,0, mountingWidth])
									cube([1, mountingThickness, (dispenserHeight - dispenserThickness)-mountingWidth*2]);
			
							}
							rotate([90,0,0])
								hull(){
									translate([i*(petriDishSize/2+dispenserThickness)+i*mountingWidth/2,
													mountingWidth+mountingScrewDiameter/2,
													-mountingThickness-space])
										cylinder(r=mountingScrewDiameter/2+space, h=mountingThickness+space*2);
									translate([i*(petriDishSize/2+dispenserThickness)+i*mountingWidth/2,	
													(dispenserHeight - dispenserThickness)-mountingWidth -mountingScrewDiameter/2,
													-mountingThickness-space])
										cylinder(r=mountingScrewDiameter/2+space, h=mountingThickness+space*2);		
							}
						}
					}
		
					//Platform support and small slider for the dishes
					translate([0, -platformSupportWidth, 0]) color("purple")
					difference(){
						cube([petriDishSize + dispenserThickness*2 , platformSupportWidth, dispenserThickness]);
						translate([-space,0,platformThickness]){
							rotate([atan((dispenserThickness-platformThickness)/platformSupportWidth),0,0])
								cube([petriDishSize + dispenserThickness*2 +space*2, platformSupportWidth*20, dispenserThickness]);
							
						}
						translate([-space, -space, -space])
							cube([petriDishSize + dispenserThickness*2 +space*2, platformSupportWidth+space, platformThickness+space]);
					}
		
				}
				//Petri "tube"
				translate([dispenserThickness-space, dispenserThickness-space, dispenserThickness])
					cube([petriDishSize+2*space,petriDishSize+2*space,dispenserHeight+dispenserThickness+space], $fn=res);
				

				//Remove some of the inner bottom to make rails for the dishes
				for(i=[0:3]){
					translate([dispenserThickness-space+((petriDishSize+space*2)/(8*2))+i*((petriDishSize+space*2)/4),-platformSupportWidth,0]){			
						hull(){
							translate([0,0,dispenserThickness-bottomRailsHeight])
								cube([(petriDishSize+space*2)/8,platformSupportWidth+0*(dispenserThickness + petriDishSize + space*2),bottomRailsHeight]);
							translate([0,platformSupportWidth+3+petriDishSize + space*2,2+0*-space])
								cube([(petriDishSize+space*2)/8,3,dispenserThickness]);
						}
						#translate([0,platformSupportWidth+3+petriDishSize + space*2,-space])
								cube([(petriDishSize+space*2)/8,3,dispenserThickness]);

					}
				}

				//Remove some of the top at the back where the sledge is placed
				translate([dispenserThickness-space,dispenserLength-endstopLength*1.5,dispenserThickness+petriDishHeight]) 
					cube([petriDishSize/2+space*2,endstopLength*2+space,dispenserThickness]);
		
				//Slits to ease adding dishes to the dispenser
				for(i=[-1:1]){
					translate([petriDishSize/2+dispenserThickness,petriDishSize/2+dispenserThickness,dispenserSliderHeight]) rotate([90,0,180+i*90]) translate([0,0,petriDishSize/2]) color("green")
						hull(){
							translate([0,slitWidth+abs(i)*-slitEkstraSideLength,0]) cylinder(r=slitWidth/2, h=dispenserThickness+space*2);
							translate([0,dispenserHeight-dispenserSliderHeight-slitWidth,0]) cylinder(r=slitWidth/2, h=dispenserThickness+space*2);
						}
				}		
						
				//Horisontal tube
				translate([dispenserThickness-space, -space, dispenserThickness-space])
					cube([petriDishSize+2*space, dispenserThickness+petriDishSize+space*2, petriDishHeight+2*space], $fn=res);
				translate([dispenserThickness-space, dispenserThickness*2+petriDishSize, dispenserThickness-space])
					cube([petriDishSize+2*space, dispenserThickness*2+petriDishSize+sliderLength+space*2, petriDishHeight+2*space], $fn=res);
				translate([dispenserThickness-space, dispenserThickness+petriDishSize-space, dispenserThickness-space])
					cube([petriDishSize+2*space, dispenserThickness+petriDishSize+space*2, sliderRailsRaise + sliderHeight + gearHeight*1.5 + space], $fn=res);
		
			//Sliding rails at side
			translate([dispenserThickness/2-space, -0.5*dispenserThickness, dispenserThickness+sliderRailsRaise-space])
				cube([sliderWidth+2*space, dispenserThickness*3+petriDishSize+sliderLength, sliderHeight+2*space], $fn=res);  
		
			//Remove half of the top at the back	
			translate([servoX, petriDishSize+2*dispenserThickness, dispenserSliderHeight-dispenserThickness-space])
				cube([petriDishSize+dispenserThickness-servoX+space, sliderLength+space, dispenserThickness+2*space], $fn=res);
		
			}	
		
			//Servo holder
			if(addServoHolder){
				translate([0, servoY, dispenserSliderHeight])	{
					difference(){ color("red")
						translate([servoX-servoMountWidth,0,0])
							cube([servoMountWidth, servoLength+servoWallThickness*2, servoWidth+servoBottomThickness], $fn=res);
						translate([-space, servoWallThickness-space/2, servoBottomThickness-space/2])
							cube([servoHeight+space*2, servoLength+space, servoWidth+space], $fn=res);
						for(i=[-1,1])
							translate([-space, servoWallThickness+servoLength/2 + i*(servoScrewsDist/2), servoBottomThickness+servoWidth/2]) rotate([0,90,0])
								cylinder(r=servoScrewDiameter/2, h=servoHeight+2*space, $fn=res);
					}
				}
			}


			//Endstop 2 mounting plate
			translate([petriDishSize+dispenserThickness+endstop2x+space,petriDishSize+dispenserThickness*2, dispenserSliderHeight])
				color("green") cube([dispenserThickness-space-endstop2x, endstopHeight+endstop2y,endstopLength+endstop2z]);
			translate([petriDishSize+dispenserThickness+endstop2x+space,petriDishSize+dispenserThickness*2, dispenserSliderHeight+endstop2z-0*endstopLength])
				color("green") cube([dispenserThickness-space-endstop2x, endstopHeight+endstop2y,endstopLength]);

	
			//Endstop3 mounting plate
			translate([0, dispenserLength-endstopLength*1.5,dispenserSliderHeight-endstopHeight+space])
				color("green") cube([dispenserThickness-space+endstop3x, endstopLength*1.5+endstop3y,endstopHeight+endstop3z]);
		}

		//Endstop at the front (endstop 2)
		translate([petriDishSize+dispenserThickness+space+endstop2x, petriDishSize+dispenserThickness*2+endstop2y, dispenserSliderHeight+endstopLength+endstop2z]) rotate([0,90,90])
			#endstop();
		
		//Endstop at the back (endstop 3)
		translate([-space+endstopWidth+dispenserThickness+endstop3x,dispenserLength+endstop3y-1, dispenserSliderHeight+endstop3z]) rotate([0,180,90])
			#endstop();
	}
}


//Movable unit
module slider(){
	difference(){
		union(){
			//The big flat part
			difference(){
				cube([sliderWidth, sliderLength, sliderHeight], $fn=res);
				translate([sliderWidth/2, 0, -space])
					cylinder(h=sliderHeight+2*space, r=(petriDishSize/2)+2*space, $fn=res);
			}
			
			color("green")
			translate([sliderWidth/2, (petriDishSize/2+space)+(sliderLength-space-petriDishSize/2)/2,sliderHeight])
				rotate([270,0,270])
					//The rack is copied and modified from http://www.thingiverse.com/thing:6011 the 3rd of April 2014
					rack(innerRadius,borders,diametralPitch,numberOfTeeth,pressureAngle,centerAngle);
			
			
			//Rails
			difference(){
				for(i=[-1,1])
					translate([(sliderWidth/2)+i*((sliderWidth/2)-dispenserThickness)-gearWidth/2, petriDishSize/2.5, sliderHeight]) color("purple")
						difference(){
							cube([gearWidth, switchBlockPlace-petriDishSize/2.5+space, gearHeight*1.5], $fn=res);
							translate([-0.5*gearWidth,0,0]) rotate([25,0,0])
								cube(gearWidth*10, $fn=res);
						}
				translate([(sliderWidth/2)-((sliderWidth/2)-dispenserThickness)-gearWidth/2-space,rightRailLength,sliderHeight]){
					rotate([65,0,0])
						cube([gearWidth+space*2,gearWidth*2,sliderLength*5]);
					cube([gearWidth+space*2,sliderLength,gearHeight*2]);
				}

			}
			
			//Block to activate stop switch
			translate([sliderWidth-switchBlockLength-dispenserThickness/2-space, switchBlockPlace, sliderHeight]) color("blue")
				cube([switchBlockLength, switchBlockWidth, switchBlockHeight], $fn=res);
		}
		
			
	}
}




module servoGear(){
	color("orange")
		sledgeGear();	
}







////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////








///****** TESTING ******///
/*
//Petri input
translate([-40,0,-150]) 
difference(){	
	mainUnit();
  	translate([-100,-90,-1]) cube([170,500,150]);
}
//Petri output
translate([0,-40,0])
intersection(){	
	translate([40,0,-30]) rotate([-90,0,0]) mainUnit();
	cube([80,30,10]);
}

//Sledge
translate([10,0,0])
intersection(){	
	translate([0,30,-165]) rotate([-270,0,0]) slider();
	cube([80,31,10]);
}*/

//translate([-100,-100,-200]) color("purple") cube (200);

//Servo holder
/*translate([70,20,-28]) rotate([0,0,90])
intersection(){	
	mainUnit();
  	translate([-118,30,28]) cube([100,65, 20]);
}*/

