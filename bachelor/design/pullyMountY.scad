blob = 0.05;

space = 1;
res = 200;

frameThickness = 30;

frameLength = 1000 + 2*frameThickness;
frameWidth = 700 + 2*frameThickness;
frameHeight = 600 + 2*frameThickness;

frameScrewDia = 8+space;


//X supported rails
mountWidth = 40;
mountHeigth = 36;//Total height (support + SBR12)
mountLength = 39;
screwDistX = 26;
screwDistY = 28;




//Motor
motorLength = 36.2+space;
motorWidth = 35+space;
motorDDia = 5+space;

screwDist = 26;
screwDia = 3+space;
circleDia = 22+space;
circleHeight = 2;

motorMountThickness = 4;

useExtraSupport = true;

//Belt
beltCenterHeight = motorMountThickness+space+motorWidth/2;//From the frame


//Pully end
//pullyCenterHeight = beltCenterHeight;
pullyCenterX = 21; //The center of the belt at the x axis
pullyInDia = 10; //???
pullyOutDia = 15; //???
pullyLength = 15; // ???
pullyMountThickness = 8;

pullyMountLength = 50;
pullyMountWidth = 70;
pullyMountHeight = 80; //Is needed??

pullyBallBearingOutDia = 10;
pullyBallBearingInDia = 5;
pullyBallBearingWidth = 4;

blockingScrewDia = 3;
blockingNutSize = 5.5+1.0;
blockingNutHeight = 4+0.5;




///////////////Print///////////////
//translate([0,0,motorLength+motorMountThickness+circleHeight]) rotate([270,0,0]) motorMountX();


/*translate([0,0,endstopMountLength]) rotate([-90,0,0])
	endstopX();
*/



//frame();
//motorMountX();
pullyMountX();
//endstopX();
//carriage();




/***PRINT***/
//rotate([90,0,0]) pullyMountX();


module frame(){
	color("silver"){
		translate([0,0,0]) cube([frameLength, frameThickness, frameThickness]);
		translate([0, frameWidth-frameThickness, 0]) cube([frameLength, frameThickness, frameThickness]);
		translate([0, 0, frameHeight-frameThickness]) cube([frameLength, frameThickness, frameThickness]);
		translate([0, frameWidth-frameThickness, frameHeight-frameThickness]) cube([frameLength, frameThickness, frameThickness]);
		
		translate([0,0,0]) cube([frameThickness,frameThickness,frameHeight]);
		translate([frameLength-frameThickness, 0, 0]) cube([frameThickness,frameThickness,frameHeight]);
		translate([0, frameWidth-frameThickness, 0]) cube([frameThickness,frameThickness,frameHeight]);
		translate([frameLength-frameThickness, frameWidth-frameThickness, 0]) cube([frameThickness,frameThickness,frameHeight]);
		
		translate([0,0,0]) cube([frameThickness,frameWidth,frameThickness]);
		translate([0,0,frameHeight-frameThickness]) cube([frameThickness,frameWidth,frameThickness]);
		translate([frameLength-frameThickness,0,0]) cube([frameThickness,frameWidth,frameThickness]);
		translate([frameLength-frameThickness,0,frameHeight-frameThickness]) cube([frameThickness,frameWidth,frameThickness]);
	}


	//Motor ends X
	translate([-(motorWidth+motorMountThickness),frameThickness+motorMountThickness,frameHeight-frameThickness])
		motorMountX();
	translate([-(motorWidth+motorMountThickness),frameWidth-motorLength-circleHeight-motorMountThickness-80,frameHeight-frameThickness]) 
		motorMountX();


	//Pully ends X
	translate([frameLength+(pullyMountWidth-frameThickness),pullyMountLength+52,frameHeight-frameThickness]) rotate([0,0,180])
		pullyMountX();
	translate([frameLength+(pullyMountWidth-frameThickness),frameWidth-pullyMountLength-25,frameHeight-frameThickness]) rotate([0,0,180])
		pullyMountX();

	//belts
	color("black"){
		hull(){
			translate([-22,85,frameHeight+pullyCenterHeight+1]) rotate([90,0,0])
				cylinder(r=pullyOutDia/2, h=5);
			translate([frameLength+22,85,frameHeight+pullyCenterHeight-2.5]) rotate([90,0,0])
				cylinder(r=pullyOutDia/2, h=5);
		}

	}


}









module motorMountX(){
	union(){
		if(useExtraSupport){
			difference(){
				union(){
					translate([0,-frameThickness-motorMountThickness,0])
						cube([motorWidth+motorMountThickness+frameThickness,frameThickness+motorMountThickness,motorWidth+motorMountThickness*2+frameThickness]);
					translate([0,0,frameThickness+motorMountThickness+motorWidth]) 
						cube([motorWidth, motorLength, motorMountThickness]);
				}
	
				translate([-blob,-frameThickness+blob,frameThickness+motorMountThickness])
					cube([motorWidth, frameThickness, motorWidth]);

				translate([motorWidth+motorMountThickness,-frameThickness-motorMountThickness-blob,-blob]) 
					cube([frameThickness+blob,frameThickness+motorMountThickness+blob*2,frameThickness+blob]);
	
				translate([motorWidth+motorMountThickness,-frameThickness,frameThickness+motorMountThickness]) 
					cube([frameThickness+blob,frameThickness,motorWidth+motorMountThickness+blob]);
				translate([motorWidth+motorMountThickness,-frameThickness-motorMountThickness-blob,motorWidth+motorMountThickness*2+frameThickness]) rotate([0,atan((motorWidth+motorMountThickness)/(frameThickness)),0]) 
					cube([frameThickness*2+blob,motorLength+circleHeight+motorMountThickness+blob*2,motorWidth*2]);
		
				translate([-blob,-frameThickness,-blob]) 
					cube([motorWidth+blob, frameThickness,frameThickness+blob]);
				translate([motorWidth,-frameThickness-motorMountThickness-blob,0]) rotate([0,180+atan(frameThickness/motorWidth),0]) 
					cube([motorWidth*2,frameThickness+motorMountThickness+blob*2,frameThickness*2]);
		
		
				//Frame screw holes
				translate([motorWidth-blob,-frameThickness/2,frameThickness/2]) rotate([0,90,0])
					cylinder(h=motorMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
				translate([motorWidth+motorMountThickness+frameThickness/2-blob,-frameThickness/2,frameThickness-blob]) rotate([0,0,0])
					cylinder(h=motorMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
			}
		} 

		difference(){
			translate([0,0,0]) 
				cube([motorWidth+motorMountThickness+frameThickness,motorLength+motorMountThickness+circleHeight,motorWidth+motorMountThickness*2+frameThickness]);
			
	
	
			translate([-blob,-blob,frameThickness+motorMountThickness]) cube([motorWidth,motorLength,motorWidth]);
			translate([0,motorLength,frameThickness+motorMountThickness]) rotate([0,0,180+atan(motorWidth/motorLength)]) cube([motorWidth*2, motorLength*2,motorWidth+motorMountThickness+blob]);
	
			translate([motorWidth/2,motorLength-blob*2,frameThickness+motorMountThickness+motorWidth/2]) rotate([270,0,0]) #cylinder(r=circleDia/2, h=circleHeight, $fn=res);
			translate([motorWidth/2,motorLength+circleHeight-blob*3,frameThickness+motorMountThickness+motorWidth/2]) rotate([270,0,0]) #cylinder(r=motorDDia/2, h=circleHeight+motorMountThickness+blob*4, $fn=res);
	
			//Screw holes
			translate([motorWidth/2-screwDist/2,motorLength-blob*2,frameThickness+motorMountThickness+motorWidth/2-screwDist/2]) rotate([270,0,0]) 
				#cylinder(r=screwDia/2, h=motorMountThickness+circleHeight+3*blob, $fn=res);
			translate([motorWidth/2-screwDist/2,motorLength-blob*2,frameThickness+motorMountThickness+motorWidth/2+screwDist/2]) rotate([270,0,0]) 
				#cylinder(r=screwDia/2, h=motorMountThickness+circleHeight+3*blob, $fn=res);
			translate([motorWidth/2+screwDist/2,motorLength-blob*2,frameThickness+motorMountThickness+motorWidth/2-screwDist/2]) rotate([270,0,0]) 
				#cylinder(r=screwDia/2, h=motorMountThickness+circleHeight+3*blob, $fn=res);
			translate([motorWidth/2+screwDist/2,motorLength-blob*2,frameThickness+motorMountThickness+motorWidth/2+screwDist/2]) rotate([270,0,0]) 
				#cylinder(r=screwDia/2, h=motorMountThickness+circleHeight+3*blob, $fn=res);
			
			//frame
			translate([motorWidth+motorMountThickness,-blob,-blob]) 
				cube([frameThickness+blob,motorLength+circleHeight+motorMountThickness+blob*2,frameThickness+blob]);
	
	
			translate([motorWidth+motorMountThickness,motorMountThickness,frameThickness+motorMountThickness]) 
				cube([frameThickness+blob,motorLength+circleHeight-motorMountThickness,motorWidth+motorMountThickness+blob]);
			translate([motorWidth+motorMountThickness,-blob,motorWidth+motorMountThickness*2+frameThickness]) rotate([0,atan((motorWidth+motorMountThickness)/(frameThickness)),0]) 
				cube([frameThickness*2+blob,motorLength+circleHeight+motorMountThickness+blob*2,motorWidth*2]);
	
			translate([-blob,motorMountThickness+circleHeight,-blob]) 
				cube([motorWidth+blob, motorLength-(motorMountThickness+circleHeight),frameThickness+blob]);
			translate([motorWidth,-blob,0]) rotate([0,180+atan(frameThickness/motorWidth),0]) 
				cube([motorWidth*2,motorLength+circleHeight+motorMountThickness+blob*2,frameThickness*2]);
	
	
			//Frame screw holes
			translate([motorWidth-blob,(motorMountThickness+circleHeight+motorLength)/2,frameThickness/2]) rotate([0,90,0])
				cylinder(h=motorMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
			translate([motorWidth+motorMountThickness+frameThickness/2-blob,(motorMountThickness+circleHeight+motorLength)/2,frameThickness-blob]) rotate([0,0,0])
				cylinder(h=motorMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
		}
	}
}








module pullyMountX(){
	difference(){
		translate([0,0,0]) 
			cube([pullyMountWidth, pullyMountLength, pullyMountHeight]);
		
		//frame
		/*translate([-blob,-blob,-blob]) 
			cube([frameThickness+blob,pullyMountLength+blob*2, frameThickness+blob]);
		*/
		translate([frameThickness,0,0])
		hull(){
			translate([pullyCenterX,(pullyMountLength-(pullyLength+2*2*space))/2,frameThickness+beltCenterHeight]) rotate([270,0,0])
				cylinder(r=pullyOutDia/2+2*2*space, h=pullyLength+2*2*space, $fn=res);
			translate([pullyMountWidth+pullyOutDia,(pullyMountLength-(pullyLength+2*2*space))/2,frameThickness+beltCenterHeight]) rotate([270,0,0])
				cylinder(r=pullyOutDia/2+2*2*space, h=pullyLength+2*2*space, $fn=res);
		}

		//Ballbearings
		translate([frameThickness,0,0])
		for(i=[-1,1])
			translate([pullyCenterX,pullyMountLength/2+i*(pullyLength/2+space),frameThickness+beltCenterHeight]) rotate([180+i*90,0,0])
				cylinder(r=pullyBallBearingOutDia/2+space/2, h=pullyBallBearingWidth, $fn=res);


		//Hole for steel cylinder
		translate([frameThickness,0,0])
		translate([pullyCenterX,(pullyMountLength-pullyLength-pullyBallBearingWidth*2)/2-space*2,frameThickness+beltCenterHeight]) rotate([270,0,0])
			cylinder(r=pullyBallBearingInDia/2+space, h=pullyMountLength+blob*2, $fn=res);


		//Hole for blocking screw
		translate([frameThickness,0,0])
		translate([pullyCenterX,pullyMountLength-pullyMountThickness,frameThickness+pullyMountThickness+pullyOutDia/2])
			cylinder(r=blockingScrewDia/2, h=pullyMountHeight, $fn=res);
		translate([frameThickness,0,0])
		hull(){
			translate([pullyCenterX,pullyMountLength-pullyMountThickness,pullyMountHeight-(pullyMountHeight-(frameThickness+beltCenterHeight))/2])
				cylinder(r=blockingNutSize/2, h=blockingNutHeight, $fn=6);
			translate([pullyCenterX,pullyMountLength+blockingNutSize,pullyMountHeight-(pullyMountHeight-(frameThickness+beltCenterHeight))/2])
				cylinder(r=blockingNutSize/2, h=blockingNutHeight, $fn=6);
		}




		translate([-blob,pullyMountThickness,frameThickness+pullyMountThickness])
			cube([frameThickness+blob,pullyMountLength-pullyMountThickness*2,pullyMountHeight-frameThickness-pullyMountThickness+blob]);

		translate([0,-blob, frameThickness+pullyMountThickness]) rotate([0,-atan((pullyMountHeight-frameThickness-pullyMountThickness)/(frameThickness)),0]) 
			cube([frameThickness*2+blob,pullyMountLength+blob*2,pullyMountHeight*2]);


		/*translate([frameThickness+pullyMountThickness,0,0])
		translate([0,pullyMountThickness,-blob]) 
			cube([pullyMountWidth-frameThickness-pullyMountThickness+blob, pullyMountLength-pullyMountThickness*2,frameThickness+blob]);
		*/
		/*translate([pullyMountThickness,0,0])
		translate([pullyMountWidth-frameThickness-pullyMountThickness,-blob,0]) rotate([0,atan(frameThickness/(pullyMountWidth-frameThickness-pullyMountThickness)),0]) 
			cube([pullyMountWidth*2,pullyMountLength+blob*2,frameThickness*2]);
*/

		//Frame screw holes
	/*	translate([frameThickness-blob, pullyMountLength/2,frameThickness/2]) rotate([0,90,0])
			cylinder(h=pullyMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
		translate([frameThickness/2,pullyMountLength/2,frameThickness-blob]) rotate([0,0,0])
			cylinder(h=pullyMountThickness+blob*2, r=frameScrewDia/2, $fn=res);
*/	}
}




/*** Endstops ***/
endstopLength = 20;
endstopWidth = 6.5;
endstopHeight = 10.2;
endstopScrewDiameter = 3;
endstopScrewDistance = 9.5;

endstopMountThickness = 4;
//endstopMountHeight = 10;
endstopMountLength = 30;
endstopMountWidth = frameThickness+endstopMountThickness*2;
endstopX_x = 10;
endstopX_z = 10;


module endstopX(){
	difference(){
		union(){
			translate([0,0,0]) 
				cube([endstopMountWidth, endstopMountLength, endstopMountThickness+frameThickness]);
			
			hull(){
				translate([0,endstopMountLength-endstopMountThickness,frameThickness+endstopMountThickness])
					cube([1,endstopMountThickness,1]);
		
				translate([endstopMountWidth+endstopHeight+endstopX_x,endstopMountLength-endstopMountThickness,frameThickness+endstopMountThickness+endstopX_z]) rotate([0,180,0])
					cube([endstopHeight,endstopMountThickness,endstopLength]);	
			}
		}
	
		translate([(endstopMountWidth-frameThickness-space)/2,-blob,-blob])
			cube([frameThickness+space, endstopMountLength+blob*2, frameThickness+blob]);
		
	
		translate([endstopMountWidth/2, (endstopMountLength-endstopMountThickness)/2, frameThickness-blob]) 
			cylinder(h=endstopMountThickness+blob*2, r=frameScrewDia/2, $fn=res);

		//Holes for screws at endstops
		#translate([endstopMountWidth+endstopX_x,endstopMountLength,frameThickness+endstopMountThickness+endstopX_z]) rotate([0,90,0])
			endstop();

	}	
}







SBRLength = 35; //????
SBRWidth = 30; //?????
SBRHeight = 25; //????
module SBR(){
	color("grey")
		cube([SBRLength, SBRWidth, SBRHeight]);
}



carriageSBRDist = 50; //Distance between SBR's to each side

carriageLength = 150;
moduleWidth = 10; //Space for extra modules like syrings, ect
moduleLength = 20; //Space for extra modules like syrings, ect
modulesAtSide = 6;
carriageWidth = carriageSBRDist + SBRWidth*2+moduleWidth*(2+2);
//carriageHeight = ;
carriageThickness = 5;

//carriage();
module carriage(){
	translate([0,moduleWidth*2,-SBRHeight])
		SBR();
	translate([carriageLength-SBRLength,moduleWidth*2,-SBRHeight])
		SBR();
	translate([0,carriageWidth-SBRWidth-moduleWidth*2,-SBRHeight])
		SBR();
	translate([carriageLength-SBRLength,carriageWidth-SBRWidth-moduleWidth*2,-SBRHeight])
		SBR();

	difference(){
		translate([0,0,0])
			cube([carriageLength, carriageWidth, carriageThickness]);
		for(i=[0:modulesAtSide-1]){
			translate([(carriageLength/modulesAtSide)*i+((carriageLength/modulesAtSide)-moduleLength)/2, moduleWidth/2,-blob])
				cube([moduleLength,moduleWidth,carriageThickness+blob*2]);
			translate([(carriageLength/modulesAtSide)*i+((carriageLength/modulesAtSide)-moduleLength)/2, carriageWidth-moduleWidth*1.5,-blob])
				cube([moduleLength,moduleWidth,carriageThickness+blob*2]);
	
		/*	translate([   +0.0, moduleWidth/2,-blob])
				#cube([moduleLength,moduleWidth,carriageThickness+blob*2]);
	*/
			
		}


	}
}





module endstop(){
	difference(){
		union(){
			//body
			color("black")
				cube([endstopLength, endstopWidth, endstopHeight]);
		
			color("silver"){
				//switch
				translate([2.9,(endstopWidth-3.8)/2,10.2]) rotate([0,-10,0])
					cube([18.2, 3.8, 0.3]);
			
				//connectors
				translate([1.4,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([10,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);	
				translate([17.5,(endstopWidth-3.3)/2,-4])
					cube([0.6,3.3,4]);			
			}
		//Holes for screws
		for(i=[-1,1])
			translate([endstopLength/2+i*endstopScrewDistance/2,-50/2,3])
				rotate([-90,0,0])
					cylinder(r=endstopScrewDiameter/2, h=50, $fn=200);
		}

	}
}


