

include <generics.scad>
length = 45+30;
width = endstopWidth;



//endstop();
extender();

module extender(){
	difference(){
		cube([length,endstopLength,width]);

		translate([length-endstopHeight,0,endstopWidth])
			rotate([90,0,90])
				#endstop();
	}


}


